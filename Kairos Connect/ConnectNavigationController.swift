import UIKit

class ConnectNavigationController: UINavigationController {
    
    fileprivate let animation = CustomNavigationAnimation()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
    }
}

extension ConnectNavigationController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        animation.transitionType = operation
        return animation
    }
}
