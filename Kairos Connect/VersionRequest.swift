import Foundation

class VersionRequest: KairosMessage {
    
    static let TYPE_ID: UInt8 = 0x69
    
    override init() {
        super.init(length: 4)
        data[3] = VersionRequest.TYPE_ID
        calculateFCS()
    }
}
