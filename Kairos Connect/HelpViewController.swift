import UIKit

class HelpViewController: UIViewController {
    @IBOutlet weak var emailButton: UIImageView!
    @IBOutlet weak var faqLabel: UILabel!
    @IBOutlet weak var twitterButton: UIImageView!
    @IBOutlet weak var pinterestButton: UIImageView!
    @IBOutlet weak var googleButton: UIImageView!
    @IBOutlet weak var facebookButton: UIImageView!
    @IBOutlet weak var instagramButton: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(emailButtonClick(_:))))
        faqLabel.isUserInteractionEnabled = true
        faqLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(faqLabelClick(_:))))
        
        twitterButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(socialClick(_:))))
        pinterestButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(socialClick(_:))))
        googleButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(socialClick(_:))))
        facebookButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(socialClick(_:))))
        instagramButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(socialClick(_:))))
    }

    @IBAction func backButtonClick() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func emailButtonClick(_ sender: UIView) {
        UIApplication.shared.open(URL(string: "mailto:info@kairoswatches.com")!, options: [:], completionHandler: nil)
    }
    
    func faqLabelClick(_ sender: UIView) {
        UIApplication.shared.open(URL(string: "https://kairoswatches.com/faq/")!, options: [:], completionHandler: nil)
    }
    
    func socialClick(_ sender: UITapGestureRecognizer) {
        switch sender.view! {
        case twitterButton:
            UIApplication.shared.open(URL(string: "https://twitter.com/KairosWatches")!, options: [:], completionHandler: nil)
        case pinterestButton:
            UIApplication.shared.open(URL(string: "https://pinterest.com/kairoswatches/")!, options: [:], completionHandler: nil)
        case googleButton:
            UIApplication.shared.open(URL(string: "https://plus.google.com/+kairoswatches")!, options: [:], completionHandler: nil)
        case facebookButton:
            UIApplication.shared.open(URL(string: "https://www.facebook.com/pg/kairoswatches")!, options: [:], completionHandler: nil)
        case instagramButton:
            let can = UIApplication.shared.canOpenURL(URL(string: "instagram://user?username=kairoswatches")!)
            if can {
                UIApplication.shared.open(URL(string: "instagram://user?username=kairoswatches")!, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.open(URL(string: "https://instagram.com/_u/kairoswatches/")!, options: [:], completionHandler: nil)
            }
        default: break
        }
    }
}
