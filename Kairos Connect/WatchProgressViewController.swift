import UIKit

class WatchProgressViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var progressView: KairosProgressView!
    @IBOutlet weak var cancelButton: KairosWireButton!
    @IBOutlet weak var retryButton: KairosWireButton!
    @IBOutlet weak var statusText: UILabel!
    @IBOutlet weak var statusMessage: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var statusProgressGroup: UIStackView!
    @IBOutlet weak var statusProgressNumber: UILabel!
    @IBOutlet weak var statusProgressPercentageSymbol: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "WatchProgressViewController", bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        nibView.frame = view.bounds
        nibView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        view.addSubview(nibView)
        view.layoutIfNeeded()
    }
    
    func transitionAnimationEnds(reverse: Bool) {
        if !reverse {
            titleLabel.alpha = 0
            closeButton.alpha = 0
            UIView.animate(withDuration: 0.5) {
                self.titleLabel.alpha = 1
                self.closeButton.alpha = 1
            }
        }
    }
    
    func transitionAnimationStarts(reverse: Bool) {
        if reverse {
            UIView.animate(withDuration: 0.5) {
                self.titleLabel.alpha = 0
                self.closeButton.alpha = 0
            }
        }
    }
    
    func setMessage(title: String, message: String) {
        statusText.text = title
        statusMessage.text = "\(message)\r\n\r\n\r\n"
    }


    @IBAction func cancelClick() {
    }
    
    @IBAction func retryClick() {
    }
    
    @IBAction func closeClick() {
    }
}
