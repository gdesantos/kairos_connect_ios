import Foundation

class StartFirmwareUpdate: KairosMessage {
    
    //The target version of this update
    let targetVersion: KairosDeviceVersion
    
    static let PACKET_SIZE: UInt32 = 250
    static let TYPE_ID: UInt8 = 0x64
    
    enum PackContent: UInt8 {
        case Bootloader = 1
        case Fsfw = 2
        case Firmware = 4
    }
    
    init(packageSize: UInt32, packageCRC: UInt32, bootloaderSize: UInt32, bootloaderCRC: UInt32, fsfwSize: UInt32, fsfwCRC: UInt32, firmwareSize: UInt32, firmwareCRC: UInt32, packContents: UInt8, versionInfo: KairosDeviceVersion) {
        
        targetVersion = versionInfo
        super.init(length: 53)
        timeout = 20000
        
        data[3] = StartFirmwareUpdate.TYPE_ID
        writeUInt32(offset: 4, value: StartFirmwareUpdate.PACKET_SIZE)
        let numberOfChunks = UInt32(ceil( Double(packageSize) / Double(StartFirmwareUpdate.PACKET_SIZE)))
        writeUInt32(offset: 8, value: numberOfChunks)
        data[12] = packContents
        writeUInt32(offset: 13, value: packageSize)
        writeUInt32(offset: 17, value: packageCRC)
        data[21] = targetVersion.packageMajor
        data[22] = targetVersion.packageMinor
        writeUInt32(offset: 23, value: bootloaderSize)
        writeUInt32(offset: 27, value: bootloaderCRC)
        data[31] = targetVersion.bootloaderMajor
        data[32] = targetVersion.bootloaderMinor
        writeUInt32(offset: 33, value: fsfwSize)
        writeUInt32(offset: 37, value: fsfwCRC)
        data[41] = targetVersion.fsfwMajor
        data[42] = targetVersion.fsfwMinor
        writeUInt32(offset: 43, value: firmwareSize)
        writeUInt32(offset: 47, value: firmwareCRC)
        data[51] = targetVersion.firmwareMajor
        data[52] = targetVersion.firmwareMinor

        calculateFCS()
    }
    
    func getNumberOfPackets() -> Int {
        var ret = 0
        ret = ret | Int(data[8])
        ret = ret | (Int(data[9]) << 8)
        ret = ret | (Int(data[10]) << 16)
        ret = ret | (Int(data[11]) << 24)
        
        return ret
    }
    
    func getPackageSize() -> Int {
        var ret = 0
        ret = ret | Int(data[13])
        ret = ret | (Int(data[14]) << 8)
        ret = ret | (Int(data[15]) << 16)
        ret = ret | (Int(data[16]) << 24)
        
        return ret
    }
    
    fileprivate func writeUInt32(offset: Int, value: UInt32) {
        data[offset] = UInt8(truncatingBitPattern: value)
        data[offset+1] = UInt8(truncatingBitPattern: value >> 8)
        data[offset+2] = UInt8(truncatingBitPattern: value >> 16)
        data[offset+3] = UInt8(truncatingBitPattern: value >> 24)
    }
}
