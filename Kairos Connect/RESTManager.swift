import Foundation
import ReachabilitySwift

class RESTManager {
    
    private let retryQueue = DispatchQueue(label: "RESTManager.retry_queue")
    private let reachability: Reachability?
    private var connectionTimeout = DEFAULT_CONNECTION_TIMEOUT
    static let DEFAULT_CONNECTION_TIMEOUT = 20000
    
    enum ResponseCode {
        case Success, ErrorGeneral, ErrorSavedForRetry
    }
    
    typealias ResponseHandler = (_ code: ResponseCode, _ data: Data?) -> Void
    
    init() {
        reachability = Reachability()
        reachability?.whenReachable = self.whenReachable
        
        do {
            try reachability?.startNotifier()
        } catch {
        }
    }
    
    func call(_ request: RESTRequest, handler: ResponseHandler?) {
        var urlString = request.serverURL + "/" + request.serviceName
        let paramsData = "method=\(request.method)\(request.buildParamsString())"
        
        print("RESTManager -> \(urlString)?\(paramsData)")
        
        if request.requestType == .Get {
            urlString += "?\(paramsData)"
        }
        
        guard let urlEncoded = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
            print("RESTManager error: URL encoding error")
            handler?(.ErrorGeneral, nil)
            return
        }
        
        guard let url = URL(string: urlEncoded) else {
            print("RESTManager error: URL encoding error")
            handler?(.ErrorGeneral, nil)
            return
        }
        
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: TimeInterval(connectionTimeout))
        urlRequest.addValue("gzip", forHTTPHeaderField: "Accept-Encoding")
        if request.requestType == .Get {
            urlRequest.httpMethod = "GET"
        } else if request.requestType == .Post {
            urlRequest.httpMethod = "POST"
            urlRequest.httpBody = paramsData.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)?.data(using: String.Encoding.utf8)
        }
        
        let dataTask = URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            if let error = error {
                print("RESTManager error: \(error.localizedDescription)")
                
                if request.saveForRetry {
                    //Save the request on disk to be sent later
                    self.retryQueue.async {
                        //request.saveForRetry = false
                        let documentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
                        var requestsBuffer = NSKeyedUnarchiver.unarchiveObject(withFile: documentsDirectory.appendingPathComponent("requests_buffer").path) as? [RESTRequest]
                        if requestsBuffer == nil {
                            requestsBuffer = [request]
                        } else {
                            requestsBuffer?.append(request)
                        }
                        NSKeyedArchiver.archiveRootObject(requestsBuffer!, toFile: documentsDirectory.appendingPathComponent("requests_buffer").path)
                    }
                    
                    handler?(.ErrorSavedForRetry, nil)
                } else {
                    handler?(.ErrorGeneral, nil)
                }
            } else {
                let strData = String(data: data!, encoding: String.Encoding.utf8)!
                print("RESTManager <- \(strData)")
                handler?(.Success, data)
            }
        }
        dataTask.resume()
    }
    
    func whenReachable(reachability: Reachability) {
        self.retryQueue.async {
            let documentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
            guard let requestsBuffer = NSKeyedUnarchiver.unarchiveObject(withFile: documentsDirectory.appendingPathComponent("requests_buffer").path) as? [RESTRequest] else {
                return
            }
            
            do {
                try FileManager().removeItem(atPath: documentsDirectory.appendingPathComponent("requests_buffer").path)
            } catch {
            }
            
            for request in requestsBuffer {
                self.call(request, handler: nil)
            }
        }
    }
}
