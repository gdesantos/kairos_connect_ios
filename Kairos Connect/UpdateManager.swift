import Foundation

class UpdateManager: NSObject {
    
    enum ResponseCode {
        case ServerVersionRetrieved
        case ErrorRetrievingDeviceVersion
        case ErrorRetrievingServerVersion
        case ErrorCheckingDeviceStatus
        case ErrorDeviceBatteryLow
        case ErrorDeviceChargerNotConnected
        case ErrorDownloadingFirmware
        case ErrorTransmittingData
        case NoUpdateNeeded
        case UpdateCanceled
        case UpdateSuccessful
    }

    enum Status {
        case RetrievingDeviceVersion
        case RetrievingServerVersion
        case CheckingDeviceReady
        case DownloadingFirmware
        case UpdatingDeviceFirmware
    }
    
    typealias RetrieveServerVersionResponseHandler = (_ code: ResponseCode, _ version: (Int, Int)?, _ firmwareURL: String?) -> Void
    
    var delegate: UpdateManagerDelegate?
    fileprivate var deviceVersion: KairosDeviceVersion?
    fileprivate var serverVersion: (Int, Int)?
    fileprivate var firmwareURL: String?
    fileprivate var cancelUpdate = false
    
    func startFWUpdate() {
        cancelUpdate = false
        
        delegate?.updateManager(self, didChangeStatus: .RetrievingDeviceVersion, withProgress: 100);
#if FAKE2
        deviceVersion = KairosDeviceVersion(packageMajor: 0, packageMinor: 0, bootloaderMajor: 0, bootloaderMinor: 0, fsfwMajor: 0, fsfwMinor: 0, firmwareMajor: 0, firmwareMinor: 0)
        stepRetrieveServerVersion()
        return
#endif
        
        AppDelegate.me.kairosDevice?.retrieveVersion(handler: { (code, version) in
            if code == .Ok {
                //Next step
                self.deviceVersion = version
                self.stepRetrieveServerVersion()
            } else {
                //Report error
                self.delegate?.updateManager(self, onError: .ErrorRetrievingDeviceVersion)
            }
        })
    }
    
    func cancelFWUpdate() {
        cancelUpdate = true
    }
    
    func retrieveServerVersion(handler: @escaping RetrieveServerVersionResponseHandler) {
        print("Retrieving FW version from server...")
        let url = URL(string: "http://notifications.kairoswatches.com/index.php?r=file/checkVersion")!
        let jsonBody: [String: Any] = ["id": 2,
                                       "method": "get_version",
                                       "params": [
                                        "major": 0,
                                        "minor": 0,
                                        "p": 21,
                                        "a": 0
            ] ,
                                       "jsonrpc": "2.0"]
        
        var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: TimeInterval(10000))
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: jsonBody)
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            do {
                guard error == nil else {
                    throw NSError(domain: "Error retrieveing server version", code: 1, userInfo: nil)
                }
                let jsonData = try? JSONSerialization.jsonObject(with: data!, options: [])
                guard let jsonDic = jsonData as? [String: Any] else {
                    throw NSError(domain: "Error parsing JSON", code: 1, userInfo: nil)
                }
                guard let majorStr = jsonDic["major"] as? String,
                    let minorStr = jsonDic["minor"] as? String,
                    let fwUrl = jsonDic["url"] as? String else {
                        throw NSError(domain: "Error parsing JSON", code: 1, userInfo: nil)
                }
                guard let majorInt = Int(majorStr),
                    let minorInt = Int(minorStr) else {
                        throw NSError(domain: "Error parsing JSON", code: 1, userInfo: nil)
                }
                
                #if FAKE2
                    print("Version: \(2).\(0)")
                    handler(.ServerVersionRetrieved, (2, 0), fwUrl)
                #else
                    print("Version: \(majorInt).\(minorInt)")
                    handler(.ServerVersionRetrieved, (majorInt, minorInt), fwUrl)
                #endif
            } catch {
                print("Error retrieving version")
                handler(.ErrorRetrievingServerVersion, nil, nil)
            }
        }
        task.resume()
    }
    
    fileprivate func stepRetrieveServerVersion() {
        if checkCancellation() {
            return
        }
        
        delegate?.updateManager(self, didChangeStatus: .RetrievingServerVersion, withProgress: 100)
        retrieveServerVersion { (code, version, url) in
            if code == .ServerVersionRetrieved {
                self.firmwareURL = url
                self.serverVersion = version
                
                if ((self.serverVersion!.0 > Int(self.deviceVersion!.packageMajor)) ||
                    ((self.serverVersion!.0 == Int(self.deviceVersion!.packageMajor)) && (self.serverVersion!.1 > Int(self.deviceVersion!.packageMinor))))
                {
                    //Next step
                    self.stepCheckDeviceReadyForFWUpdate()
                } else {
                    //Up to date
                    self.delegate?.updateManager(self, didFinishUpdateWithResponseCode: .NoUpdateNeeded, updatedVersion: version!)
                }
            } else {
                //Report error
                self.delegate?.updateManager(self, onError: .ErrorRetrievingServerVersion)
            }
        }
    }
    
    fileprivate func stepCheckDeviceReadyForFWUpdate() {
        if checkCancellation() {
            return
        }

        delegate?.updateManager(self, didChangeStatus: .CheckingDeviceReady, withProgress: 100)
        #if FAKE2
            stepDownloadingFW()
            return
        #endif
        AppDelegate.me.kairosDevice!.isReadyForFWUpdate { (code, status) in
            if code == .Ok {
                switch status {
                case .BatteryLow:
                    self.delegate?.updateManager(self, onError: .ErrorDeviceBatteryLow)
                    
                case .ChargerNotConnected:
                    self.delegate?.updateManager(self, onError: .ErrorDeviceChargerNotConnected)
                    
                case .Ready:
                    self.stepDownloadingFW()
                    
                case .Unknown:
                    self.delegate?.updateManager(self, onError: .ErrorCheckingDeviceStatus)
                }
            } else {
                self.delegate?.updateManager(self, onError: .ErrorCheckingDeviceStatus)
            }
        }
    }
    
    fileprivate func stepDownloadingFW() {
        if checkCancellation() {
            return
        }

        delegate?.updateManager(self, didChangeStatus: .DownloadingFirmware, withProgress: 0)
        let url = URL(string: firmwareURL!)!
        let request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: TimeInterval(10000))
        let session = URLSession(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: nil)
        session.downloadTask(with: request).resume()
    }
    
    fileprivate func stepUpdateDeviceFW() {
        if checkCancellation() {
            return
        }

        delegate?.updateManager(self, didChangeStatus: .UpdatingDeviceFirmware, withProgress: 0)
        #if FAKE2
            AppDelegate.me.settingsManager.setLastSeenVersion(major: serverVersion!.0, minor: serverVersion!.1)
            delegate?.updateManager(self, didFinishUpdateWithResponseCode: .UpdateSuccessful, updatedVersion: serverVersion!)
            return
        #endif
        let documentsUrl:URL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL!
        let fileUrl = documentsUrl.appendingPathComponent("kairos_fw_update.zip")
        AppDelegate.me.kairosDevice!.updateFirmware(fileURL: fileUrl, version: deviceVersion!) { (code, progress) in
            if code == .Ok {
                if progress == 100 {
                    self.delegate?.updateManager(self, didFinishUpdateWithResponseCode: .UpdateSuccessful, updatedVersion: self.serverVersion!)
                } else if self.cancelUpdate {
                    self.cancelUpdate = false
                    AppDelegate.me.kairosDevice!.cancelFirmwareUpdate()
                } else {
                    self.delegate?.updateManager(self, didChangeStatus: .UpdatingDeviceFirmware, withProgress: progress)
                }
            } else if code == .FirmwareUpdateCancelled {
                self.delegate?.updateManager(self, didFinishUpdateWithResponseCode: .UpdateCanceled, updatedVersion: (0,0))
            } else {
                self.delegate?.updateManager(self, onError: .ErrorTransmittingData)
            }
        }
    }
    
    fileprivate func checkCancellation() -> Bool {
        if cancelUpdate {
            delegate?.updateManager(self, didFinishUpdateWithResponseCode: .UpdateCanceled, updatedVersion: (0,0))
            return true
        } else {
            return false
        }
    }
}

extension UpdateManager: URLSessionDownloadDelegate {
 
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        let documentsUrl:URL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL!
        let destinationFileUrl = documentsUrl.appendingPathComponent("kairos_fw_update.zip")
        let fileManager = FileManager.default
        do {
            try fileManager.removeItem(at: destinationFileUrl)
            try fileManager.removeItem(at: documentsUrl.appendingPathComponent("kairos_fw_update"))
        } catch {
            //Non critical, the file probably doesn't exists
        }
        
        do {
            try fileManager.copyItem(at: location, to: destinationFileUrl)
            stepUpdateDeviceFW()
        } catch (let error) {
            print("Error creating file \(destinationFileUrl) : \(error.localizedDescription)")
            self.delegate?.updateManager(self, onError: .ErrorDownloadingFirmware)
        }
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        if cancelUpdate {
            downloadTask.cancel()
            delegate?.updateManager(self, didFinishUpdateWithResponseCode: .UpdateCanceled, updatedVersion: (0,0))
        } else {
            delegate?.updateManager(self, didChangeStatus: .DownloadingFirmware, withProgress: Double(totalBytesWritten)/Double(totalBytesExpectedToWrite) * 100)
        }
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        if let error = error {
            print("Error downloading firmware: \(error.localizedDescription)")
            self.delegate?.updateManager(self, onError: .ErrorDownloadingFirmware)
        }
    }
}

protocol UpdateManagerDelegate: class {
    func updateManager(_ updateManager: UpdateManager, didChangeStatus status: UpdateManager.Status, withProgress progress: Double)
    func updateManager(_ updateManager: UpdateManager, didFinishUpdateWithResponseCode responseCode: UpdateManager.ResponseCode, updatedVersion: (Int, Int))
    func updateManager(_ updateManager: UpdateManager, onError error: UpdateManager.ResponseCode)
}
