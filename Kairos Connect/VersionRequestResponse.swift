import Foundation

class VersionRequestResponse: KairosResponse {
    
    public private(set) var version: KairosDeviceVersion
    
    override init(baseData: [UInt8], extendedData: [UInt8]) {
        version = KairosDeviceVersion(packageMajor: extendedData[1],
                                      packageMinor: extendedData[2],
                                      bootloaderMajor: extendedData[3],
                                      bootloaderMinor: extendedData[4],
                                      fsfwMajor: extendedData[5],
                                      fsfwMinor: extendedData[6],
                                      firmwareMajor: extendedData[7],
                                      firmwareMinor: extendedData[8])
        super.init(baseData: baseData, extendedData: extendedData)
    }
}
