import UIKit

class DashboardNavigationController: UINavigationController {
    
    fileprivate let animation = CustomNavigationAnimation()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
    }
}

extension DashboardNavigationController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        if toVC is FirmwareUpdateViewController || fromVC is FirmwareUpdateViewController {
            animation.transitionType = operation
            return animation
        } else {
            return nil
        }
    }
}
