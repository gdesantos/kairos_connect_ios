import Foundation

class SettingsManager {
    
    private let KEY_ASSOCIATED_DEVICE_NAME = "key_associated_device_name"
    private let KEY_ASSOCIATED_DEVICE_ADDRESS = "key_associated_device_address"
    private let KEY_MUST_SHOW_HOME_ACTIVITY = "key_show_home_activity"
    private let KEY_LAST_SEEN_VERSION_MAJOR = "key_last_seen_version_major"
    private let KEY_LAST_SEEN_VERSION_MINOR = "key_last_seen_version_minor"
    private let KEY_UPDATE_AVALIABLE = "key_update_avaliable"
    
    private let KEY_WEATHER_NOTIFICATIONS_ENABLED = "weather_notifications_enabled";
    private let KEY_WEATHER_NOTIFICATIONS_LAST_SENT = "setting_weather_notification_last_sent";
    private let KEY_WEATHER_NOTIFICATIONS_STEP_IN_SECONDS = "setting_weather_notification_step_in_seconds";
    private let KEY_WEATHER_NOTIFICATIONS_PROVIDER = "setting_weather_notification_provider";
    private let KEY_WEATHER_NOTIFICATIONS_UNITS = "setting_weather_notification_units";
    
    init() {
        UserDefaults.standard.register(defaults: [KEY_ASSOCIATED_DEVICE_NAME: "",
                                                  KEY_ASSOCIATED_DEVICE_ADDRESS: "",
                                                  KEY_MUST_SHOW_HOME_ACTIVITY: true,
                                                  KEY_LAST_SEEN_VERSION_MAJOR: 0,
                                                  KEY_LAST_SEEN_VERSION_MINOR: 0,
                                                  KEY_UPDATE_AVALIABLE: false,
                                                  KEY_WEATHER_NOTIFICATIONS_ENABLED: false,
                                                  KEY_WEATHER_NOTIFICATIONS_PROVIDER: String(describing: OpenWeatherMapProvider.self),
                                                  KEY_WEATHER_NOTIFICATIONS_STEP_IN_SECONDS: 6*3600,
                                                  KEY_WEATHER_NOTIFICATIONS_LAST_SENT: -1,
                                                  KEY_WEATHER_NOTIFICATIONS_UNITS: WeatherProvider.TemperatureUnits.Celsius.rawValue]);
    }
    
    var associatedDeviceName: String {
        get {
            return UserDefaults.standard.string(forKey: KEY_ASSOCIATED_DEVICE_NAME)!
        }
        set {
            UserDefaults.standard.set(newValue, forKey: KEY_ASSOCIATED_DEVICE_NAME)
        }
    }
    
    var associatedDeviceAddress: String {
        get {
            return UserDefaults.standard.string(forKey: KEY_ASSOCIATED_DEVICE_ADDRESS)!
        }
        set {
            UserDefaults.standard.set(newValue, forKey: KEY_ASSOCIATED_DEVICE_ADDRESS)
        }
    }
    
    var showHomeActivity: Bool {
        get {
            return UserDefaults.standard.bool(forKey: KEY_MUST_SHOW_HOME_ACTIVITY)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: KEY_MUST_SHOW_HOME_ACTIVITY)
        }
    }
    
    var updateAvaliable: Bool {
        get {
            return UserDefaults.standard.bool(forKey: KEY_UPDATE_AVALIABLE)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: KEY_UPDATE_AVALIABLE)
        }
    }
    
    func setLastSeenVersion(major: Int, minor: Int) {
        UserDefaults.standard.set(major, forKey: KEY_LAST_SEEN_VERSION_MAJOR)
        UserDefaults.standard.set(minor, forKey: KEY_LAST_SEEN_VERSION_MINOR)
    }
    
    func getLastSeenVersion() -> (Int, Int) {
        let major = UserDefaults.standard.integer(forKey: KEY_LAST_SEEN_VERSION_MAJOR)
        let minor = UserDefaults.standard.integer(forKey: KEY_LAST_SEEN_VERSION_MINOR)
        return (major, minor)
    }
    
    var weatherNotificationsEnabled: Bool {
        get {
            return UserDefaults.standard.bool(forKey: KEY_WEATHER_NOTIFICATIONS_ENABLED)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: KEY_WEATHER_NOTIFICATIONS_ENABLED)
            UserDefaults.standard.set(-1, forKey: KEY_WEATHER_NOTIFICATIONS_LAST_SENT)
        }
    }
    
    var weatherStepInSeconds: Int {
        get {
            return UserDefaults.standard.integer(forKey: KEY_WEATHER_NOTIFICATIONS_STEP_IN_SECONDS)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: KEY_WEATHER_NOTIFICATIONS_STEP_IN_SECONDS)
        }
    }
    
    var weatherUnits: WeatherProvider.TemperatureUnits {
        get {
            return WeatherProvider.TemperatureUnits(rawValue: UserDefaults.standard.integer(forKey: KEY_WEATHER_NOTIFICATIONS_UNITS))!
        }
        set {
            UserDefaults.standard.set(newValue.rawValue, forKey: KEY_WEATHER_NOTIFICATIONS_UNITS)
        }
    }
    
    var weatherProvider: String {
        get {
            return UserDefaults.standard.string(forKey: KEY_WEATHER_NOTIFICATIONS_PROVIDER)!
        }
        set {
            UserDefaults.standard.set(newValue, forKey: KEY_WEATHER_NOTIFICATIONS_PROVIDER)
        }
    }
    
    var weatherLastNotificationSent: Double {
        get {
            return UserDefaults.standard.double(forKey: KEY_WEATHER_NOTIFICATIONS_LAST_SENT)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: KEY_WEATHER_NOTIFICATIONS_LAST_SENT)
        }
    }
}
