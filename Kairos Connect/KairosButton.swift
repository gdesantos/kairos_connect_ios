import UIKit

class KairosButton: UIButton {
    
    private let normalColor = UIColor(red: 0.0, green: 0xD5/255.0, blue: 0xD8/255.0, alpha: 1)
    private let selectedColor = UIColor(red: 0x01/255.0, green: 0xAB/255.0, blue: 0xAD/255.0, alpha: 1)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.postInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.postInit()
    }
    
    private func postInit() {
        layer.cornerRadius = 21
        self.backgroundColor = normalColor
        addTarget(self, action: #selector(buttonTap(sender:)), for: .touchDown)
        addTarget(self, action: #selector(buttonUnTap(sender:)), for: [.touchUpInside, .touchUpOutside, .touchCancel])
    }
    
    func buttonTap(sender: AnyObject) {
        self.backgroundColor = selectedColor
    }
    
    func buttonUnTap(sender: AnyObject) {
        self.backgroundColor = normalColor
    }

}
