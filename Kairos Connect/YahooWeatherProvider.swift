import Foundation
import CoreLocation

class YahooWeatherProvider: WeatherProvider, WeatherProviderProtocol {
    
    func getURL(location: CLLocationCoordinate2D) -> String {
        let temperatureChar: Character
        
        switch units {
        case .Celsius: temperatureChar = "c"
        case .Farenheit: temperatureChar = "f"
        }

        return "https://query.yahooapis.com/v1/public/yql?format=json&q=select%20units,%20item.condition%20from%20weather.forecast%20where%20woeid%20in%20(SELECT%20woeid%20FROM%20geo.places%20WHERE%20text=%22(\(location.latitude),\(location.longitude))%22)%20and%20u=%27\(temperatureChar)%27"
    }
    
    func parse(response: Data) -> String? {
        do {
            let json = try JSONSerialization.jsonObject(with: response) as? [String: Any]
            
            guard let jQuery = json?["query"] as? [String: Any],
                let jResults = jQuery["results"] as? [String: Any],
                let jChannel = jResults["channel"] as? [String: Any],
                let jItem = jChannel["item"] as? [String: Any],
                let jCondition = jItem["condition"] as? [String: Any],
                let jUnits = jChannel["units"] as? [String: Any],
                let text = jCondition["text"] as? String,
                let temp = jCondition["temp"] as? String,
                let temperature = jUnits["temperature"] as? String else{
                    
                return nil
            }

            return text + ", " + NSLocalizedString("Temperature", comment: "") + " \(temp)º\(temperature)"
        } catch {
            return nil
        }
    }
}
