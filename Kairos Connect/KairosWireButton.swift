import UIKit
import QuartzCore

class KairosWireButton: UIButton {
    
    fileprivate let backColor = UIColor(red: 0x22/255.0, green: 0x22/255.0, blue: 0x22/255.0, alpha: 1)
    fileprivate var mainColor: UIColor!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.postInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.postInit()
    }
    
    private func postInit() {
        layer.cornerRadius = 21
        addTarget(self, action: #selector(buttonTap(sender:)), for: .touchDown)
        addTarget(self, action: #selector(buttonUnTap(sender:)), for: [.touchUpInside, .touchUpOutside, .touchCancel])
        mainColor = titleLabel?.textColor!
        layer.borderColor = mainColor.cgColor
        layer.borderWidth = 2
        setTitleColor(mainColor, for: .normal)
        setTitleColor(backColor, for: .highlighted)
    }
    
    func buttonTap(sender: AnyObject) {
        backgroundColor = mainColor
    }
    
    func buttonUnTap(sender: AnyObject) {
        backgroundColor = UIColor.clear
    }
    
}
