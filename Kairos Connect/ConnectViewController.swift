import UIKit
import CoreBluetooth

class ConnectViewController: WatchProgressViewController {
    
    var btDevice: CBPeripheral!
    fileprivate var kairosDevice: KairosDevice?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if btDevice.name != nil {
            titleLabel.text = btDevice.name
        } else {
            titleLabel.text = btDevice.identifier.description
        }
        
        cancelButton.isHidden = true
        statusProgressGroup.isHidden = true
    }
    
    override func transitionAnimationEnds(reverse: Bool) {
        super.transitionAnimationEnds(reverse: reverse)
        if !reverse {
            connect()
        }
    }

    override func retryClick() {
        super.retryClick()
        connect()
    }
    
    override func closeClick() {
        super.closeClick()
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    fileprivate func connect() {
        retryButton.alpha = 0
        closeButton.alpha = 0
        setMessage(title: NSLocalizedString("Connecting device...", comment: ""),
                   message: NSLocalizedString("Connecting to your device. This process may take a moment, please wait", comment: ""))
        progressView.status = .ProgressIndeterminate
        progressView.progress = 0
        
        kairosDevice = KairosDevice.getInstance(name: btDevice.name, uuid: btDevice.identifier)
        kairosDevice?.retrieveVersion(handler: { (code, version) in
            if code == .Ok {
                self.connectionSuccess()
            } else {
                self.connectionError()
            }
        })
    }
    
    fileprivate func connectionSuccess() {
        print("Connection success")
        AppDelegate.me.kairosDevice = kairosDevice
        setMessage(title: NSLocalizedString("Connected!", comment: ""), message: "")
        progressView.status = .Success
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(1200), execute: {
            AppDelegate.me.settingsManager.showHomeActivity = false
            self.performSegue(withIdentifier: "from_connect_to_dashboard", sender: self)
        })
    }
    
    fileprivate func connectionError() {
        print("Connection error")
        kairosDevice?.disconnect()
        kairosDevice = nil
        DispatchQueue.main.async {
            self.setMessage(title: NSLocalizedString("Connection error", comment: ""),
                            message: NSLocalizedString("Please check if your device is turned on and properly connected", comment: ""))
            self.progressView.status = .Error
            self.closeButton.alpha = 1
            self.retryButton.alpha = 1
        }
    }
}
