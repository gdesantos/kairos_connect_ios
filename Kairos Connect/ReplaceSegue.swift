import UIKit

class ReplaceSegue: UIStoryboardSegue {
    
    override func perform() {
        AppDelegate.me.window?.rootViewController = destination
    }
}
