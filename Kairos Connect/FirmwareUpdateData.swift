import Foundation

class FirmwareUpdateData: KairosMessage {
    
    static let TYPE_ID: UInt8 = 0x65
    
    init(sourceData: [UInt8], offset: Int, length: Int) {
        super.init(length: UInt8(4 + length))
        data[2] = 0x00
        data[3] = FirmwareUpdateData.TYPE_ID
        
        for index in offset...offset+length-1 {
            data[4+index-offset] = sourceData[index]
        }
        
        calculateFCS()
    }
}
