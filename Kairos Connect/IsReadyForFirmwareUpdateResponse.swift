import Foundation

class IsReadyForFirmwareUpdateResponse: KairosResponse {
    
    enum Status: Int {
        case Ready = 0
        case BatteryLow = 1
        case ChargerNotConnected = 2
        case Unknown = 3
    }
    
    var status: Status {
        switch Int(data[5]) {
        case Status.Ready.rawValue: return Status.Ready
        case Status.BatteryLow.rawValue: return Status.BatteryLow
        case Status.ChargerNotConnected.rawValue: return Status.ChargerNotConnected
        default: return Status.Unknown
        }
    }
}
