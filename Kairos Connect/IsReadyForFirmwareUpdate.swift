import Foundation

class IsReadyForFirmwareUpdate: KairosMessage {
    
    static let TYPE_ID: UInt8 = 0x68
    
    override init() {
        super.init(length: 4)
        data[3] = IsReadyForFirmwareUpdate.TYPE_ID
        calculateFCS()
    }
}
