import Foundation

import CoreBluetooth

class KairosDeviceScanner : NSObject {
    
    fileprivate let btCentralManager: CBCentralManager
    weak var delegate: KairosDeviceScannerDelegate?
    var isScanning = false
    
    override init() {
        btCentralManager = AppDelegate.me.btCentralManager
        super.init()
        btCentralManager.delegate = self
    }
    
    func start() {
        print("Scanner started")
        isScanning = true
        let kairosServiceUUID = CBUUID(string: "F4F2B816-A092-466E-BC76-320462D6341A")
        let devices = btCentralManager.retrieveConnectedPeripherals(withServices: [kairosServiceUUID])
        for device in devices {
            centralManager(btCentralManager, didDiscover: device, advertisementData: [:], rssi: 0)
        }
        btCentralManager.scanForPeripherals(withServices: [kairosServiceUUID], options: nil)
    }

    func stop() {
        print("Scanner stopped")
        isScanning = false
        btCentralManager.stopScan()
    }
}

extension KairosDeviceScanner: CBCentralManagerDelegate {
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        if central.state == .poweredOn {
            print("Bluetooth powered on")
        } else {
            print("Unexpected bluetooth state")
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        print("Device discovered: \(String(describing: peripheral.name))")
        
        if peripheral.name != nil {
            if !peripheral.name!.hasPrefix("KW") {
                //Skip devices that does not match our BT name pattern
                return;
            }
        }
        
        delegate?.kairosDeviceScanner(self, didFindDevice: peripheral)
    }
}

protocol KairosDeviceScannerDelegate: class {
    func kairosDeviceScanner(_ scanner: KairosDeviceScanner, didFindDevice device: CBPeripheral)
}

