import UIKit

class WeatherSettingsViewController: UIViewController {
    
    @IBOutlet weak var setting1h: UIView!
    @IBOutlet weak var setting6h: UIView!
    @IBOutlet weak var setting12h: UIView!
    @IBOutlet weak var setting24h: UIView!
    @IBOutlet weak var settingCelsius: UIStackView!
    @IBOutlet weak var settingFarenheit: UIStackView!
    @IBOutlet weak var selectedProviderLabel: UILabel!
    @IBOutlet weak var selectProviderGroup: UIStackView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupFrequencySetting(setting1h)
        setupFrequencySetting(setting6h)
        setupFrequencySetting(setting12h)
        setupFrequencySetting(setting24h)
        
        switch AppDelegate.me.settingsManager.weatherStepInSeconds {
        case 60*60: enableFrequencySetting(setting1h, enable: true)
        case 6*60*60: enableFrequencySetting(setting6h, enable: true)
        case 12*60*60: enableFrequencySetting(setting12h, enable: true)
        case 24*60*60: enableFrequencySetting(setting24h, enable: true)
        default: break
        }
        
        setupUnitsSetting(settingCelsius)
        setupUnitsSetting(settingFarenheit)
        
        switch AppDelegate.me.settingsManager.weatherUnits {
        case .Celsius: enableUnitSetting(settingCelsius, enable: true)
        case .Farenheit: enableUnitSetting(settingFarenheit, enable: true)
        }
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(selectProviderGroupTap(_:)))
        selectProviderGroup.addGestureRecognizer(gestureRecognizer)
        
        selectedProviderLabel.text = AppDelegate.me.settingsManager.weatherProvider
        
        switch AppDelegate.me.settingsManager.weatherProvider {
        case String(describing: OpenWeatherMapProvider.self): selectedProviderLabel.text = "OpenWeatherMap"
        case String(describing: YahooWeatherProvider.self): selectedProviderLabel.text = "Yahoo Weather"
        case String(describing: DarkSkyProvider.self): selectedProviderLabel.text = "Dark Sky"
        default: selectedProviderLabel.text = "Unknown"
        }
    }
    
    private func setupFrequencySetting(_ settingView: UIView) {
        settingView.layer.borderColor = UIColor(red: 0x85/255.0, green: 0x85/255.0, blue: 0x85/255.0, alpha: 1).cgColor
        settingView.layer.borderWidth = 2
        settingView.layer.cornerRadius = settingView.frame.size.width/2.0
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(frequencySettingTap(_:)))
        settingView.addGestureRecognizer(gestureRecognizer)
    }
    
    private func setupUnitsSetting(_ settingView: UIStackView) {
        settingView.arrangedSubviews[0].layer.borderColor = UIColor(red: 0x85/255.0, green: 0x85/255.0, blue: 0x85/255.0, alpha: 1).cgColor
        settingView.arrangedSubviews[0].layer.borderWidth = 2
        settingView.arrangedSubviews[0].layer.cornerRadius = settingView.frame.size.width/2.0
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(unitSettingTap(_:)))
        settingView.addGestureRecognizer(gestureRecognizer)
    }
    
    @IBAction func backButtonClick() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func frequencySettingTap(_ sender: UITapGestureRecognizer) {
        enableFrequencySetting(setting1h, enable: sender.view == setting1h)
        enableFrequencySetting(setting6h, enable: sender.view == setting6h)
        enableFrequencySetting(setting12h, enable: sender.view == setting12h)
        enableFrequencySetting(setting24h, enable: sender.view == setting24h)
    }
    
    func unitSettingTap(_ sender: UITapGestureRecognizer) {
        enableUnitSetting(settingCelsius, enable: sender.view == settingCelsius)
        enableUnitSetting(settingFarenheit, enable: sender.view == settingFarenheit)
    }
    
    func enableFrequencySetting(_ settingView: UIView, enable: Bool) {
        if enable {
            settingView.layer.borderColor = UIColor(red: 0x1A/255.0, green: 0xCD/255.0, blue: 0xCF/255.0, alpha: 1).cgColor
            settingView.layer.backgroundColor = UIColor(red: 0x14/255.0, green: 0xC4/255.0, blue: 0xCE/255.0, alpha: 0x19/255.0).cgColor
            (settingView.subviews[0] as! UILabel).textColor = UIColor.neonBlue()
            
            switch settingView {
            case setting1h: AppDelegate.me.settingsManager.weatherStepInSeconds = 60*60
            case setting6h: AppDelegate.me.settingsManager.weatherStepInSeconds = 6*60*60
            case setting12h: AppDelegate.me.settingsManager.weatherStepInSeconds = 12*60*60
            case setting24h: AppDelegate.me.settingsManager.weatherStepInSeconds = 24*60*60
            default: break
            }
        } else {
            settingView.layer.borderColor = UIColor(red: 0x85/255.0, green: 0x85/255.0, blue: 0x85/255.0, alpha: 1).cgColor
            settingView.layer.backgroundColor = UIColor.clear.cgColor
            (settingView.subviews[0] as! UILabel).textColor = UIColor(red: 0x98/255.0, green: 0x98/255.0, blue: 0x98/255.0, alpha: 1)
        }
    }
    
    func enableUnitSetting(_ settingView: UIView, enable: Bool) {
        let settingView = settingView as! UIStackView
        
        if enable {
            settingView.arrangedSubviews[0].layer.borderColor = UIColor(red: 0x1A/255.0, green: 0xCD/255.0, blue: 0xCF/255.0, alpha: 1).cgColor
            settingView.arrangedSubviews[0].layer.backgroundColor = UIColor(red: 0x14/255.0, green: 0xC4/255.0, blue: 0xCE/255.0, alpha: 0x19/255.0).cgColor
            (settingView.arrangedSubviews[0].subviews[0] as! UILabel).textColor = UIColor.neonBlue()
            (settingView.arrangedSubviews[1] as! UILabel).textColor = UIColor.neonBlue()
            
            switch settingView {
            case settingCelsius: AppDelegate.me.settingsManager.weatherUnits = .Celsius
            case settingFarenheit: AppDelegate.me.settingsManager.weatherUnits = .Farenheit
            default: break
            }
        } else {
            settingView.arrangedSubviews[0].layer.borderColor = UIColor(red: 0x85/255.0, green: 0x85/255.0, blue: 0x85/255.0, alpha: 1).cgColor
            settingView.arrangedSubviews[0].layer.backgroundColor = UIColor.clear.cgColor
            (settingView.arrangedSubviews[0].subviews[0] as! UILabel).textColor = UIColor(red: 0x98/255.0, green: 0x98/255.0, blue: 0x98/255.0, alpha: 1)
        }
    }
    
    func selectProviderGroupTap(_ sender: UITapGestureRecognizer) {
        performSegue(withIdentifier: "providers_popover_segue", sender: self)
    }
    
    func popupProviderClick(_ sender: UIButton) {
        switch sender.titleLabel!.text! {
        case "OpenWeatherMap": AppDelegate.me.settingsManager.weatherProvider = String(describing: OpenWeatherMapProvider.self)
        case "Dark Sky": AppDelegate.me.settingsManager.weatherProvider = String(describing: DarkSkyProvider.self)
        case "Yahoo Weather": AppDelegate.me.settingsManager.weatherProvider = String(describing: YahooWeatherProvider.self)
        default: break
        }
        
        selectedProviderLabel.text = sender.titleLabel!.text
        dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "providers_popover_segue" {
            segue.destination.preferredContentSize = segue.destination.view.subviews[0].frame.size
            let controller = segue.destination.popoverPresentationController
            controller?.sourceView = selectedProviderLabel
            controller?.sourceRect = selectedProviderLabel.bounds
            controller?.backgroundColor = segue.destination.view.backgroundColor
            controller?.delegate = self

            let stackView = segue.destination.view.subviews[0].subviews[0] as! UIStackView
            for button in stackView.arrangedSubviews {
                let button = button as! UIButton
                button.addTarget(self, action: #selector(popupProviderClick(_:)), for: .touchUpInside)
            }
        }
    }
}

extension WeatherSettingsViewController: UIPopoverPresentationControllerDelegate {
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}
