import Foundation

class CancelFirmwareUpdate: KairosMessage {
    
    static let TYPE_ID: UInt8 = 0x66
    
    override init() {
        super.init(length: 4)
        data[3] = CancelFirmwareUpdate.TYPE_ID
        calculateFCS()
    }
}
