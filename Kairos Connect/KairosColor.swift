import UIKit

extension UIColor {
    
    class func darkGrey() -> UIColor {
        return UIColor(red: 0x22/255.0, green: 0x22/255.0, blue: 0x22/255.0, alpha: 1)
    }
    
    class func blackGrey() -> UIColor {
        return UIColor(red: 0x1B/255.0, green: 0x1B/255.0, blue: 0x1B/255.0, alpha: 1)
    }

    class func neonBlue() -> UIColor {
        return UIColor(red: 0x00/255.0, green: 0xD5/255.0, blue: 0xD8/255.0, alpha: 1)
    }

    class func neonGreen() -> UIColor {
        return UIColor(red: 0x0B/255.0, green: 0xE5/255.0, blue: 0x6D/255.0, alpha: 1)
    }

    class func neonYellow() -> UIColor {
        return UIColor(red: 0xFF/255.0, green: 0xF7/255.0, blue: 0x13/255.0, alpha: 1)
    }

    class func neonPink() -> UIColor {
        return UIColor(red: 0xFF/255.0, green: 0x23/255.0, blue: 0x66/255.0, alpha: 1)
    }
}
