import UIKit

class FirmwareUpdateViewController: WatchProgressViewController {
    
    fileprivate let updateManager = UpdateManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        updateManager.delegate = self
        titleLabel.text = NSLocalizedString("FIRMWARE UPDATE", comment: "")
        progressView.image.image = #imageLiteral(resourceName: "dashboard_device")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.isIdleTimerDisabled = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        UIApplication.shared.isIdleTimerDisabled = false
    }
    
    override func transitionAnimationEnds(reverse: Bool) {
        super.transitionAnimationEnds(reverse: reverse)
        if !reverse {
            startFwUpdate()
        }
    }
    
    override func closeClick() {
        super.closeClick()
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    override func cancelClick() {
        super.cancelClick()
        setMessage(title: NSLocalizedString("Cancelling...", comment: ""),
                   message: NSLocalizedString("Trying to cancel the current operation. This can take a while", comment: ""))
        showActionButton(retry: false, cancel: false)
        updateManager.cancelFWUpdate()
    }
    
    override func retryClick() {
        super.retryClick()
        startFwUpdate()
    }
    
    fileprivate func startFwUpdate() {
        showActionButton(retry: false, cancel: true)
        statusProgressGroup.alpha = 0
        progressView.status = .ProgressIndeterminate
        setMessage(title: NSLocalizedString("Checking...", comment: ""),
                   message: NSLocalizedString("Do not switch off or reboot your Kairos device while downloading and installing firmware update", comment: ""))
        updateManager.startFWUpdate()
    }
    
    fileprivate func showActionButton(retry: Bool, cancel: Bool) {
        retryButton.alpha = retry ? 1 : 0
        cancelButton.alpha = cancel ? 1 : 0
        
        retryButton.isHidden = !retry
        cancelButton.isHidden = !cancel
        closeButton.isHidden = !retry
        
        if !retry && !cancel {
            cancelButton.isHidden = false
        }
    }
        
    fileprivate func updateProgress(progress: Double) {
        statusProgressNumber.text = "\(Int(progress))"
        let color: UIColor
        
        if progress <= 33 {
            color = UIColor.neonBlue()
        } else if progress <= 66 {
            color = UIColor(red: 0x0C/255.0, green: 0xEF/255.0, blue: 0xAF/255.0, alpha: 1)
        } else {
            color = UIColor.neonGreen()
        }
        
        statusProgressNumber.textColor = color
        statusProgressPercentageSymbol.textColor = color
        progressView.progress = Float(progress)
    }
}

extension FirmwareUpdateViewController: UpdateManagerDelegate {
    func updateManager(_ updateManager: UpdateManager, didChangeStatus status: UpdateManager.Status, withProgress progress: Double) {
        DispatchQueue.main.async {
            switch status {
            case .RetrievingDeviceVersion:
                self.setMessage(title: NSLocalizedString("Checking...", comment: ""),
                                message: NSLocalizedString("Do not switch off or reboot your Kairos device while downloading and installing firmware update", comment: ""))
                
            case .RetrievingServerVersion:
                self.setMessage(title: NSLocalizedString("Checking...", comment: ""),
                                message: NSLocalizedString("Do not switch off or reboot your Kairos device while downloading and installing firmware update", comment: ""))
                
            case .CheckingDeviceReady:
                self.setMessage(title: NSLocalizedString("Checking...", comment: ""),
                                message: NSLocalizedString("Do not switch off or reboot your Kairos device while downloading and installing firmware update", comment: ""))
                
            case .DownloadingFirmware:
                self.progressView.status = .Progress
                self.statusProgressGroup.alpha = 1
                self.updateProgress(progress: progress)
                self.setMessage(title: NSLocalizedString("Downloading...", comment: ""),
                                message: NSLocalizedString("Do not switch off or reboot your Kairos device while downloading and installing firmware update", comment: ""))
                
            case .UpdatingDeviceFirmware:
                if progress == 0 {
                    self.statusProgressGroup.alpha = 0
                    self.progressView.status = .ProgressIndeterminate
                    self.setMessage(title: NSLocalizedString("Decompressing...", comment: ""),
                                    message: NSLocalizedString("Do not switch off or reboot your Kairos device while downloading and installing firmware update", comment: ""))
                } else {
                    self.statusProgressGroup.alpha = 1
                    self.progressView.status = .Progress
                    self.updateProgress(progress: progress)
                    self.setMessage(title: NSLocalizedString("Installing...", comment: ""),
                                    message: NSLocalizedString("Do not switch off or reboot your Kairos device while downloading and installing firmware update", comment: ""))
                }
            }
        }
    }
    
    func updateManager(_ updateManager: UpdateManager, didFinishUpdateWithResponseCode responseCode: UpdateManager.ResponseCode, updatedVersion: (Int, Int)) {
        DispatchQueue.main.async {
            switch responseCode {
            case .NoUpdateNeeded:
                //This should never happen
                AppDelegate.me.settingsManager.updateAvaliable = false
                self.progressView.status = .Success
                self.showActionButton(retry: false, cancel: false)
                self.closeButton.isHidden = false
                self.setMessage(title: NSLocalizedString("No update needed", comment: ""), message: "")
                
            case .UpdateCanceled:
                _ = self.navigationController?.popViewController(animated: true)
                
            case .UpdateSuccessful:
                AppDelegate.me.settingsManager.updateAvaliable = false
                self.updateProgress(progress: 100)
                self.progressView.status = .Success
                self.showActionButton(retry: false, cancel: false)
                self.closeButton.isHidden = false
                self.setMessage(title: NSLocalizedString("Successfully updated", comment: ""),
                                message: NSLocalizedString("Your watch will reboot now and will reconnect to the application", comment: ""))
                
            default:
                return
            }
        }
    }
    
    func updateManager(_ updateManager: UpdateManager, onError error: UpdateManager.ResponseCode) {
        DispatchQueue.main.async {
            self.progressView.status = .Error
            self.statusProgressGroup.alpha = 0
            self.showActionButton(retry: true, cancel: false)
            
            switch error {
            case .ErrorRetrievingDeviceVersion:
                self.setMessage(title: NSLocalizedString("Error", comment: ""),
                                message: NSLocalizedString("Error retrieving device version. Please try again", comment: ""))

            case .ErrorRetrievingServerVersion:
                self.setMessage(title: NSLocalizedString("Error", comment: ""),
                                message: NSLocalizedString("Error retrieving server version. Please try again", comment: ""))

            case .ErrorDeviceBatteryLow:
                self.setMessage(title: NSLocalizedString("Error", comment: ""),
                                message: NSLocalizedString("Battery of the device must be over 50% to start the update", comment: ""))

            case .ErrorDeviceChargerNotConnected:
                self.setMessage(title: NSLocalizedString("Error", comment: ""),
                                message: NSLocalizedString("The device must be connected to the charger to start the update", comment: ""))

            case .ErrorCheckingDeviceStatus:
                self.setMessage(title: NSLocalizedString("Error", comment: ""),
                                message: NSLocalizedString("Error in checking device status. Please try again", comment: ""))

            case .ErrorDownloadingFirmware:
                self.setMessage(title: NSLocalizedString("Error", comment: ""),
                                message: NSLocalizedString("Firmware download error. Please try again", comment: ""))

            case .ErrorTransmittingData:
                self.setMessage(title: NSLocalizedString("Error", comment: ""),
                                message: NSLocalizedString("Data transmission error. Please try again", comment: ""))

            default:
                self.setMessage(title: NSLocalizedString("Error", comment: ""),
                                message: NSLocalizedString("Unknown error", comment: ""))
            }
        }
    }
}
