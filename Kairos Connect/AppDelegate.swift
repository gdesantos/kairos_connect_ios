import UIKit
import CoreBluetooth
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var btCentralManager: CBCentralManager!
    var settingsManager: SettingsManager!
    var kairosDevice: KairosDevice? {
        didSet {
            if kairosDevice != nil {
                settingsManager.associatedDeviceName = kairosDevice!.name
                settingsManager.associatedDeviceAddress = kairosDevice!.address.uuidString
            } else {
                settingsManager.associatedDeviceName = ""
                settingsManager.associatedDeviceAddress = ""
            }

        }
    }
    var weatherManager: WeatherManager!
    static var me: AppDelegate!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        Fabric.with([Crashlytics.self])
        AppDelegate.me = self
            
        settingsManager = SettingsManager()
        weatherManager = WeatherManager()
        btCentralManager = CBCentralManager()

        #if FAKE
            self.kairosDevice = KairosDevice.getInstance(name: "Debug", uuid: UUID.init(uuidString: "E621E1F8-C36C-495A-93FC-0C247A3E6E5F")!)
            settingsManager.setLastSeenVersion(major: 0, minor: 0)
        #endif

        if !settingsManager.associatedDeviceName.isEmpty && !settingsManager.associatedDeviceAddress.isEmpty {
            self.kairosDevice = KairosDevice.getInstance(name: settingsManager.associatedDeviceName,
                                                         uuid: UUID.init(uuidString: settingsManager.associatedDeviceAddress)!)
            print("Cached device found: \(String(describing: self.kairosDevice?.name))")
        } else {
            self.kairosDevice = nil
            print("Cached device not found")
        }
        
        return true
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        weatherManager.didRegisterForRemoteNotificationsWithDeviceToken(deviceToken: deviceToken)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        weatherManager.didFailToRegisterForRemoteNotificationsWithError(error: error)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        weatherManager.didReceiveRemoteNotification(userInfo: userInfo, fetchCompletionHandler: completionHandler)
    }
}

