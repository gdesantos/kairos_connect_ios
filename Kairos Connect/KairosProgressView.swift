import UIKit

class KairosProgressView: UIView {
    
    enum Status {
        case Progress, ProgressIndeterminate, Success, Error
    }
    
    @IBOutlet weak var imageTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageRightConstraint: NSLayoutConstraint!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var statusIcon: UIImageView!
    
    fileprivate var view: UIView!
    //Alpha circle to show on success/error
    fileprivate var statusCircleView: UIView!
    fileprivate var circleBoundingRect = CGRect()
    fileprivate var displayLink: CADisplayLink!
    fileprivate var indeterminateAnimationAccumulator = 0
    fileprivate var connectionErrorAnimation: [UIImage]
    fileprivate var connectionSuccessAnimation: [UIImage]
    var status: Status = .Progress {
        didSet {
            if oldValue == status {
                return
            }
            
            switch (status) {
            case .Progress:
                statusIcon.isHidden = true
                statusCircleView.alpha = 0
                displayLink.invalidate()
                progress = 0
            case .ProgressIndeterminate:
                statusIcon.isHidden = true
                statusCircleView.alpha = 0
                progress = 0
                indeterminateAnimationAccumulator = 0
                displayLink = CADisplayLink(target: self, selector: #selector(indeterminateAnimationUpdate))
                displayLink.preferredFramesPerSecond = 60
                displayLink.add(to: .main, forMode: .defaultRunLoopMode)
            case .Error:
                displayLink.invalidate()
                statusIcon.isHidden = false
                statusIcon.image = #imageLiteral(resourceName: "connection_error_animation8")
                statusIcon.animationImages = connectionErrorAnimation
                statusIcon.animationDuration = 0.280
                statusIcon.animationRepeatCount = 1
                statusIcon.startAnimating()
                statusCircleView.backgroundColor = UIColor.neonYellow()
                UIView.animate(withDuration: 0.280, animations: {
                    self.statusCircleView.alpha = 0.2
                })
                setNeedsDisplay()
            case .Success:
                displayLink.invalidate()
                statusIcon.isHidden = false
                statusIcon.image = #imageLiteral(resourceName: "connection_success_animation8")
                statusIcon.animationImages = connectionSuccessAnimation
                statusIcon.animationDuration = 0.280
                statusIcon.animationRepeatCount = 1
                statusIcon.startAnimating()
                statusCircleView.backgroundColor = UIColor.neonGreen()
                UIView.animate(withDuration: 0.280, animations: {
                    self.statusCircleView.alpha = 0.2
                })
                setNeedsDisplay()
            }
        }
    }
    var progress: Float = 0.0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    fileprivate static let CIRCLE_PADDING: CGFloat = 0
    fileprivate static let CIRCLE_STROKE_WIDTH: CGFloat = 8
    fileprivate static let IMAGE_PADDING: CGFloat = 65
    fileprivate static let INDETERMINATE_DURATION = 15
    
    override init(frame:CGRect) {
        connectionErrorAnimation = [ #imageLiteral(resourceName: "connection_error_animation0"),
                                     #imageLiteral(resourceName: "connection_error_animation1"),
                                     #imageLiteral(resourceName: "connection_error_animation2"),
                                     #imageLiteral(resourceName: "connection_error_animation3"),
                                     #imageLiteral(resourceName: "connection_error_animation4"),
                                     #imageLiteral(resourceName: "connection_error_animation5"),
                                     #imageLiteral(resourceName: "connection_error_animation6"),
                                     #imageLiteral(resourceName: "connection_error_animation7"),
                                     #imageLiteral(resourceName: "connection_error_animation8") ]
        
        connectionSuccessAnimation = [ #imageLiteral(resourceName: "connection_success_animation0"),
                                       #imageLiteral(resourceName: "connection_success_animation1"),
                                       #imageLiteral(resourceName: "connection_success_animation2"),
                                       #imageLiteral(resourceName: "connection_success_animation3"),
                                       #imageLiteral(resourceName: "connection_success_animation4"),
                                       #imageLiteral(resourceName: "connection_success_animation5"),
                                       #imageLiteral(resourceName: "connection_success_animation6"),
                                       #imageLiteral(resourceName: "connection_success_animation7"),
                                       #imageLiteral(resourceName: "connection_success_animation8")]
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder:NSCoder) {
        connectionErrorAnimation = [ #imageLiteral(resourceName: "connection_error_animation0"),
                                     #imageLiteral(resourceName: "connection_error_animation1"),
                                     #imageLiteral(resourceName: "connection_error_animation2"),
                                     #imageLiteral(resourceName: "connection_error_animation3"),
                                     #imageLiteral(resourceName: "connection_error_animation4"),
                                     #imageLiteral(resourceName: "connection_error_animation5"),
                                     #imageLiteral(resourceName: "connection_error_animation6"),
                                     #imageLiteral(resourceName: "connection_error_animation7"),
                                     #imageLiteral(resourceName: "connection_error_animation8") ]
        
        connectionSuccessAnimation = [ #imageLiteral(resourceName: "connection_success_animation0"),
                                       #imageLiteral(resourceName: "connection_success_animation1"),
                                       #imageLiteral(resourceName: "connection_success_animation2"),
                                       #imageLiteral(resourceName: "connection_success_animation3"),
                                       #imageLiteral(resourceName: "connection_success_animation4"),
                                       #imageLiteral(resourceName: "connection_success_animation5"),
                                       #imageLiteral(resourceName: "connection_success_animation6"),
                                       #imageLiteral(resourceName: "connection_success_animation7"),
                                       #imageLiteral(resourceName: "connection_success_animation8")]
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "KairosProgressView", bundle: bundle)
        view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = bounds
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        addSubview(view)
    }
    
    override func layoutSubviews() {
        if bounds.width >= bounds.height {
            let diff = bounds.width - bounds.height
            circleBoundingRect = CGRect(x: KairosProgressView.CIRCLE_PADDING + diff/2,
                                        y: KairosProgressView.CIRCLE_PADDING,
                                        width: bounds.width - 2*(KairosProgressView.CIRCLE_PADDING + diff/2),
                                        height: bounds.width - 2*(KairosProgressView.CIRCLE_PADDING + diff/2))
        } else {
            circleBoundingRect = CGRect(x: KairosProgressView.CIRCLE_PADDING,
                                        y: KairosProgressView.CIRCLE_PADDING,
                                        width: bounds.width - KairosProgressView.CIRCLE_PADDING*2,
                                        height: bounds.width - KairosProgressView.CIRCLE_PADDING*2)
        }
        
        imageLeftConstraint.constant = circleBoundingRect.origin.x + KairosProgressView.IMAGE_PADDING
        imageTopConstraint.constant = circleBoundingRect.origin.y + KairosProgressView.IMAGE_PADDING
        imageRightConstraint.constant = imageLeftConstraint.constant
        imageBottomConstraint.constant = bounds.height - (circleBoundingRect.origin.y + circleBoundingRect.size.height - KairosProgressView.IMAGE_PADDING)
        
        statusCircleView = UIView(frame: circleBoundingRect)
        statusCircleView.backgroundColor = UIColor.red
        statusCircleView.alpha = 0
        view.addSubview(statusCircleView)

        let maskPath = CGMutablePath()
        maskPath.addEllipse(in: statusCircleView.bounds)
        let shapeLayer = CAShapeLayer(layer: statusCircleView.layer)
        shapeLayer.path = maskPath
        shapeLayer.fillColor = UIColor.black.cgColor
        statusCircleView.layer.mask = shapeLayer
    }
 
    override func draw(_ rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()!
        
        if status == .Progress || status == .ProgressIndeterminate {
            let backgroundArc = CGMutablePath()
            backgroundArc.addArc(center: CGPoint(x: circleBoundingRect.origin.x + circleBoundingRect.size.width/2,
                                                 y: circleBoundingRect.origin.y + circleBoundingRect.size.height/2),
                                 radius: circleBoundingRect.size.width/2 - KairosProgressView.CIRCLE_STROKE_WIDTH/2,
                                 startAngle: -CGFloat(Float.pi/2),
                                 endAngle: -CGFloat(Float.pi/2) + CGFloat(2 * Float.pi - 0.0001),
                                 clockwise: false)
            let strokedBackgroundArc = backgroundArc.copy(strokingWithWidth: KairosProgressView.CIRCLE_STROKE_WIDTH, lineCap: .butt, lineJoin: .miter, miterLimit: 10)
            
            context.saveGState()
            
            context.addPath(strokedBackgroundArc)
            context.setFillColor(UIColor.darkGrey().cgColor)
            context.fillPath()
            
            context.restoreGState()

            let arc = CGMutablePath()
            arc.addArc(center: CGPoint(x: circleBoundingRect.origin.x + circleBoundingRect.size.width/2,
                                       y: circleBoundingRect.origin.y + circleBoundingRect.size.height/2),
                       radius: circleBoundingRect.size.width/2 - KairosProgressView.CIRCLE_STROKE_WIDTH/2,
                       startAngle: -CGFloat(Float.pi/2),
                       endAngle: -CGFloat(Float.pi/2) + CGFloat(progress * 2.0 * Float.pi / 100.0),
                       clockwise: false)
            
            let strokedArc = arc.copy(strokingWithWidth: KairosProgressView.CIRCLE_STROKE_WIDTH, lineCap: .butt, lineJoin: .miter, miterLimit: 10)
            
            context.addPath(strokedArc)
            context.clip()

            #imageLiteral(resourceName: "progress_gradient").draw(in: circleBoundingRect)
        }
    }
    
    func indeterminateAnimationUpdate(displayLink: CADisplayLink) {
        let totalCalls = displayLink.preferredFramesPerSecond * KairosProgressView.INDETERMINATE_DURATION
        indeterminateAnimationAccumulator += 1
        if(indeterminateAnimationAccumulator > totalCalls) {
            indeterminateAnimationAccumulator = 0
        }
        
        progress = Float(indeterminateAnimationAccumulator) * 100.0 / Float(totalCalls)
    }
}
