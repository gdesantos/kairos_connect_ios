import UIKit
import CoreBluetooth

class ScanViewController: TransitableToProgressViewController {
    @IBOutlet weak var scanImageView: UIImageView!
    @IBOutlet weak var scanArrowsButton: UIButton!
    @IBOutlet weak var scanCollectionView: UICollectionView!
    @IBOutlet weak var searchingLayout: UIView!
    @IBOutlet weak var timeoutLayout: UIView!
    @IBOutlet weak var backgroundAnimationView: UIView!
    
    fileprivate var scanner: KairosDeviceScanner!
    fileprivate var kairosDevice: KairosDevice?
    fileprivate var scannedDevices: [CBPeripheral] = []
    fileprivate var rotateAnimation: CABasicAnimation!
    fileprivate var timeoutTimer: Timer?
    fileprivate var selectedItemList: UICollectionView?
    
    fileprivate static let SCAN_TIMEOUT: TimeInterval = 30
    fileprivate static let CELL_LABEL_TAG_ID = 1000
    fileprivate static let CELL_CONNECT_IMAGE_TAG_ID = 1001
    fileprivate static let CELL_DEVICE_IMAGE_TAG_ID = 1002

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //This is to fix a bug: when going to background and resuming, the arrows animation stops
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive(sender:)), name: .UIApplicationDidBecomeActive, object: nil)
        
        scanner = KairosDeviceScanner()
        scanner.delegate = self
        
        //Prepare the rotate animation
        rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(Float.pi * 2.0)
        rotateAnimation.duration = 1.5
        rotateAnimation.repeatCount = .infinity
        
        //Create the mask for the timeout layout triangle
        for constraint in timeoutLayout.superview!.constraints {
            if (constraint.firstItem === timeoutLayout && constraint.firstAttribute == .top) || (constraint.secondItem === timeoutLayout && constraint.secondAttribute == .top) {
                let maskPath = UIBezierPath()
                maskPath.move(to: CGPoint(x: 0, y: -constraint.constant))
                maskPath.addLine(to: CGPoint(x: view.frame.size.width, y: 0))
                maskPath.addLine(to: CGPoint(x: view.frame.size.width, y: timeoutLayout.frame.size.height))
                maskPath.addLine(to: CGPoint(x: 0, y: timeoutLayout.frame.size.height))
                maskPath.close()
                let maskShape = CAShapeLayer()
                maskShape.frame = self.view.bounds
                maskShape.path = maskPath.cgPath
                maskShape.fillRule = kCAFillRuleEvenOdd
                timeoutLayout.layer.mask = maskShape
                break
            }
        }

        showBottonView(viewToShow: nil)
        startScan()
    }
    
    func applicationDidBecomeActive(sender: AnyObject) {
        if scanner.isScanning {
            scanArrowsButton.layer.add(rotateAnimation, forKey: nil)
        }
    }
    
    func startScan() {
        //Clear list
        scannedDevices.removeAll()
        scanCollectionView.reloadData()
        //Start the rotation animation of the arrows
        scanArrowsButton.layer.add(rotateAnimation, forKey: nil)
        //Start the main scan animation
        scanImageView.image = UIImage.animatedImageNamed("link_animation_ellipses", duration: 1.5)
        //Show the "searching" bottom view
        showBottonView(viewToShow: searchingLayout)
        //Start the timeout counter
        timeoutTimer = Timer.scheduledTimer(timeInterval: ScanViewController.SCAN_TIMEOUT, target: self, selector: #selector(timeoutReached(timer:)), userInfo: nil, repeats: false);
        //Start the BT scanner
        self.scanner.start()
    }
    
    func stopScan() {
        //Stop the BT scanner
        scanner.stop()
        //Remove the animation of the arrows
        scanArrowsButton.layer.removeAllAnimations()
        //Stop the main scan animation
        scanImageView.image = UIImage.init(named: "link_animation_ellipses0")
        //Stop the timeout counter
        timeoutTimer?.invalidate()
        
    }
    
    func timeoutReached(timer: Timer) {
        stopScan()
        if scannedDevices.count == 0 {
            showBottonView(viewToShow: timeoutLayout)
        }
    }
    
    fileprivate func showBottonView(viewToShow: UIView?) {
        scanCollectionView.isHidden = viewToShow != scanCollectionView
        searchingLayout.isHidden = viewToShow != searchingLayout
        timeoutLayout.isHidden = viewToShow != timeoutLayout
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        (segue.destination as! ConnectViewController).btDevice = sender as! CBPeripheral
    }
    
    @IBAction func scanClick() {
        if !scanner.isScanning {
            startScan()
        }
    }

    @IBAction func getHelpClick() {
        UIApplication.shared.open(URL(string: "http://kairostechsupport.com")!, options: [:]) { (bool) in
        }
    }
    
    override func transitionAnimationStarts(reverse: Bool) {
        super.transitionAnimationStarts(reverse: reverse)
        if reverse {
            UIView.animate(withDuration: CustomNavigationAnimation.TRANSITION_DURATION, animations: {
                self.backgroundAnimationView.alpha = 0
            }) { (bool) in
                self.backgroundAnimationView.isUserInteractionEnabled = false
            }
        }
    }
    
    override func transitionAnimationEnds(reverse: Bool) {
        super.transitionAnimationEnds(reverse: reverse)
        if reverse {
            let connectImage = selectedItemList!.viewWithTag(ScanViewController.CELL_CONNECT_IMAGE_TAG_ID) as! UIImageView
            connectImage.isHidden = false
        }
    }
}

extension ScanViewController: KairosDeviceScannerDelegate {
    
    func kairosDeviceScanner(_ scanner: KairosDeviceScanner, didFindDevice device: CBPeripheral) {
        showBottonView(viewToShow: scanCollectionView)
        var found = false
        for peripheral in scannedDevices {
            if device.name != nil && peripheral.name != nil && device.name == peripheral.name {
                found = true
                break
            }
        }

        if !found {
            scannedDevices.append(device)
            scanCollectionView.reloadData()
        }
    }
}

extension ScanViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        stopScan()
        selectedItemList = collectionView
        let connectImage = selectedItemList!.viewWithTag(ScanViewController.CELL_CONNECT_IMAGE_TAG_ID) as! UIImageView
        connectImage.isHidden = true
        backgroundAnimationView.isUserInteractionEnabled = true
        originOfAnimationImageView = collectionView.viewWithTag(ScanViewController.CELL_DEVICE_IMAGE_TAG_ID) as? UIImageView
        UIView.animate(withDuration: CustomNavigationAnimation.TRANSITION_DURATION, animations: {
            self.backgroundAnimationView.alpha = 1
        }) { (bool) in
        }
        self.performSegue(withIdentifier: "from_scan_to_connect", sender: scannedDevices[indexPath.row])
    }
}

extension ScanViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return scannedDevices.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = scanCollectionView.dequeueReusableCell(withReuseIdentifier: "scan_item_cell", for: indexPath)
        let label = cell.viewWithTag(ScanViewController.CELL_LABEL_TAG_ID) as! UILabel
        let btDevice = scannedDevices[indexPath.row]
        if btDevice.name != nil {
            label.text = btDevice.name
        } else {
            label.text = btDevice.identifier.description
        }
        return cell
    }
}

extension ScanViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}
