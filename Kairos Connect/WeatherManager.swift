import UIKit
import UserNotifications
import CoreLocation

class WeatherManager: NSObject {
    
    fileprivate var pushToken: String?
    fileprivate let restManager = RESTManager()
    fileprivate let locationManager = CLLocationManager()
    fileprivate var notificationPostponed: UNNotificationContent?
    
    fileprivate static let pushRegistrationHost = "http://push.kairoswatches.com"
    #if DEBUG
        fileprivate static let pushRegistrationService = "KairosMobileServiceDev.php"
    #else
        fileprivate static let pushRegistrationService = "KairosMobileService.php"
    #endif
    
    override init() {
        super.init()
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
        }
        UIApplication.shared.registerForRemoteNotifications()
        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillResignActive(sender:)), name: .UIApplicationWillResignActive, object: nil)
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
        locationManager.requestAlwaysAuthorization()
        locationManager.startMonitoringSignificantLocationChanges()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func didRegisterForRemoteNotificationsWithDeviceToken(deviceToken: Data) {
        pushToken = deviceToken.map { String(format: "%02hhx", $0) }.joined()
        print("Push token: \(pushToken!)")
        refreshToken()
    }
    
    func didFailToRegisterForRemoteNotificationsWithError(error: Error) {
        print("Push registration failed")
    }
    
    func didReceiveRemoteNotification(userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        print("Remote notification received")
        let result = checkForWeather()
        completionHandler(result ? .newData : .noData)
    }
    
    func applicationWillResignActive(sender: AnyObject) {
        if notificationPostponed != nil {
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 2, repeats: false)
            let request = UNNotificationRequest(identifier: "weather_notification", content: notificationPostponed!, trigger: trigger)
            UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
            notificationPostponed = nil
        }
    }
    
    func enableWeatherNotifications() {
        AppDelegate.me.settingsManager.weatherNotificationsEnabled = true
        refreshToken()
        DispatchQueue.global(qos: .background).async {
            _ = self.checkForWeather()
        }
    }
    
    func disableWeatherNotifications() {
        guard let pushToken = pushToken else {
            return
        }
        
        AppDelegate.me.settingsManager.weatherNotificationsEnabled = false
        let request = RESTRequest(serverURL: WeatherManager.pushRegistrationHost,
                                  serviceName: WeatherManager.pushRegistrationService,
                                  method: "disableWeatherNotifications")
        request.params["token"] = pushToken
        request.saveForRetry = true
        restManager.call(request, handler: nil)
    }
    
    fileprivate func refreshToken() {
        guard let pushToken = pushToken else {
            return
        }
        
        if !AppDelegate.me.settingsManager.weatherNotificationsEnabled {
            return
        }
        
        let request = RESTRequest(serverURL: WeatherManager.pushRegistrationHost,
                                  serviceName: WeatherManager.pushRegistrationService,
                                  method: "enableWeatherNotifications")
        request.params["token"] = pushToken
        request.saveForRetry = true
        restManager.call(request, handler: nil)
    }
    
    fileprivate func sendKeepAlive() {
        guard let pushToken = pushToken else {
            return
        }
        
        let request = RESTRequest(serverURL: WeatherManager.pushRegistrationHost,
                                  serviceName: WeatherManager.pushRegistrationService,
                                  method: "keepAlive")
        request.params["token"] = pushToken
        request.saveForRetry = true
        restManager.call(request, handler: nil)
    }
    
    fileprivate func checkForWeather() -> Bool {
        let settingsManager = AppDelegate.me.settingsManager!
        let currentTime = Date().timeIntervalSince1970 * 1000
        
        if !settingsManager.weatherNotificationsEnabled {
            print("checkForWeather: weather notifications are disabled")
            return false
        }
        
        if (currentTime - settingsManager.weatherLastNotificationSent) < Double(settingsManager.weatherStepInSeconds*1000) {
            //Too soon
            print("checkForWeather: too soon")
            return false
        }
        
        guard let lastLocationCoordinates = locationManager.location?.coordinate else {
            //Invalid location
            print("checkForWeather: invalid location")
            return false
        }
        
        let content = UNMutableNotificationContent()
        content.title = NSLocalizedString("Current weather", comment: "")
        
        let provider: WeatherProviderProtocol
        if settingsManager.weatherProvider == String(describing: YahooWeatherProvider.self) {
            provider = YahooWeatherProvider(withUnits: settingsManager.weatherUnits)
        } else if settingsManager.weatherProvider == String(describing: OpenWeatherMapProvider.self) {
            provider = OpenWeatherMapProvider(withUnits: settingsManager.weatherUnits)
        } else if settingsManager.weatherProvider == String(describing: DarkSkyProvider.self) {
            provider = DarkSkyProvider(withUnits: settingsManager.weatherUnits)
        } else {
            provider = YahooWeatherProvider(withUnits: settingsManager.weatherUnits)
        }
        
        let weatherURL = URL(string: provider.getURL(location: lastLocationCoordinates))!
        var weatherData: Data?
        var numTry = 0
        
        repeat {
            weatherData = try? Data(contentsOf: weatherURL)
            numTry += 1
        } while (weatherData == nil) && (numTry < 3)
        
        if weatherData == nil {
            print("checkForWeather: invalid weather data")
            return false
        }
        
        guard let parsedData = provider.parse(response: weatherData!) else {
            print("checkForWeather: invalid weather data")
            return false
        }
        
        print(parsedData)
        
        content.body = parsedData
        sendKeepAlive()
        
        if UIApplication.shared.applicationState == .active {
            //Foreground
            notificationPostponed = content
        } else {
            //Background
            let request = UNNotificationRequest(identifier: "\(Int(Date().timeIntervalSince1970))", content: content, trigger: nil)
            UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        }
        
        settingsManager.weatherLastNotificationSent = currentTime
        
        return true
    }
}

extension WeatherManager: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        _ = checkForWeather()
    }
}

extension Data {
    func hexEncodedString() -> String {
        return map { String(format: "%02hhx", $0) }.joined()
    }
}
