import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var blurViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var logoStackView: UIStackView!
    @IBOutlet weak var logoStackViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var logoStackViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var logoStackViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var moreInfoStackView: UIStackView!
    @IBOutlet weak var startStackView: UIStackView!
    
    private var coverView: UIView!
    private var coverViewTopLayoutContraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let blurEffect = UIBlurEffect(style: .regular)
        blurView.effect = blurEffect
    
        let maskPath = UIBezierPath()
        maskPath.move(to: CGPoint(x: 0, y: -blurViewTopConstraint.constant))
        maskPath.addLine(to: CGPoint(x: view.frame.size.width, y: 0))
        maskPath.addLine(to: CGPoint(x: view.frame.size.width, y: self.blurView.frame.size.height*3))
        maskPath.addLine(to: CGPoint(x: 0, y: self.blurView.frame.size.height*3))
        maskPath.close()
        
        let maskShape = CAShapeLayer()
        maskShape.frame = self.view.bounds
        maskShape.path = maskPath.cgPath
        maskShape.fillRule = kCAFillRuleEvenOdd
        
        let maskView = UIView(frame: self.view.bounds)
        maskView.backgroundColor = UIColor.black
        maskView.layer.mask = maskShape
        blurView.mask = maskView
        
        //create cover view
        coverView = UIView()
        coverView.backgroundColor = UIColor(red: 0x99/255.0, green: 0x99/255.0, blue: 0x99/255.0, alpha: 0x99/255.0)
        coverView.isHidden = true
        coverView.isOpaque = false
        self.view.insertSubview(coverView, at: 1)
        coverView.translatesAutoresizingMaskIntoConstraints = false
        coverViewTopLayoutContraint = NSLayoutConstraint(item: coverView, attribute: .top, relatedBy: .equal, toItem: blurView, attribute: .top, multiplier: 1.0, constant: 0.0)
        coverViewTopLayoutContraint.isActive = true
        NSLayoutConstraint(item: coverView, attribute: .leading, relatedBy: .equal, toItem: blurView, attribute: .leading, multiplier: 1.0, constant: 0.0).isActive = true
        NSLayoutConstraint(item: coverView, attribute: .trailing, relatedBy: .equal, toItem: blurView, attribute: .trailing, multiplier: 1.0, constant: 0.0).isActive = true
        NSLayoutConstraint(item: coverView, attribute: .bottom, relatedBy: .equal, toItem: blurView, attribute: .bottom, multiplier: 1.0, constant: 0.0).isActive = true
        coverView.layer.mask = maskShape
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

    }

    @IBAction func moreInfoClick() {
        UIApplication.shared.open(URL(string: "https://kairoswatches.com/")!, options: [:]) { (bool) in
        }
    }
    
    @IBAction func startClick() {
        self.coverView.isHidden = false
        UIView.animate(withDuration: 0.6, delay: 0, options: [], animations: {
            let factor = 150.0 / self.logoStackViewWidthConstraint.constant
            self.logoStackViewTopConstraint.constant = 20
            self.logoStackViewWidthConstraint.constant *= factor
            self.logoStackViewHeightConstraint.constant *= factor
            self.coverView.backgroundColor = UIColor(red: 0x22/255.0, green: 0x22/255.0, blue: 0x22/255.0, alpha: 1.0)
            self.coverViewTopLayoutContraint.constant -= self.coverView.frame.origin.y - self.blurViewTopConstraint.constant
            self.view.layoutIfNeeded()
        }) { (bool) in
            UIView.animate(withDuration: 0.6, animations: { 
                self.logoStackView.arrangedSubviews[1].alpha = 0
            }, completion: { (bool) in
                self.performSegue(withIdentifier: "from_home_to_scan", sender: self)
            })
        }
        
        UIView.animate(withDuration: 0.6) {
            self.blurView.alpha = 0
            self.moreInfoStackView.alpha = 0
            self.startStackView.alpha = 0
        }
    }
}
