import Foundation

struct KairosDeviceVersion {
    let packageMajor: UInt8
    let packageMinor: UInt8
    let bootloaderMajor: UInt8
    let bootloaderMinor: UInt8
    let fsfwMajor: UInt8
    let fsfwMinor: UInt8
    let firmwareMajor: UInt8
    let firmwareMinor: UInt8
}
