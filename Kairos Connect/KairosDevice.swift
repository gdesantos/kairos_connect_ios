import Foundation
import CoreBluetooth
import Zip

class KairosDevice {
    
    let name: String
    let address: UUID
    
    fileprivate let commsManager: BTCommsManager
    fileprivate var currentFirmwareUpdateInfo: StartFirmwareUpdate?
    fileprivate var currentFirmwareUpdateDataPacket = 0
    fileprivate var currentFirmwareUpdateResponseHandler: UpdateFirmwareResponseHandler?
    fileprivate var currentFirmwareUpdateData: [UInt8]?
    fileprivate var cancellingFirmwareUpdate = false
    
    enum ResponseCode {
        case Ok, FirmwareUpdateCancelled, Error
    }
    
    typealias GenericResponseHandler = (_ code: ResponseCode) -> Void
    typealias RetrieveVersionResponseHandler = (_ code: ResponseCode, _ version: KairosDeviceVersion?) -> Void
    typealias IsReadyForFWUpdateResponseHandler = (_ code: ResponseCode, _ status: IsReadyForFirmwareUpdateResponse.Status) -> Void
    typealias UpdateFirmwareResponseHandler = (_ code: ResponseCode, _ progress: Double) -> Void
    
    init(name: String, address: UUID) {
        self.name = name
        self.address = address
        self.commsManager = BTCommsManager(address: address)
    }
 
    static func getInstance(name: String?, uuid: UUID) -> KairosDevice {
        if name == nil {
            return KairosWatch(name: uuid.uuidString, uuid: uuid)
        } else {
            return KairosWatch(name: name!, uuid: uuid)
        }
    }
    
    func disconnect() {
        commsManager.disconnect()
        AppDelegate.me.weatherManager.disableWeatherNotifications()
    }
    
    func retrieveVersion(handler: @escaping RetrieveVersionResponseHandler) {
        let message = VersionRequest()
        commsManager.sendMessage(message: message) { (message, code, response) in
            if code == .Ok {
                let version = (response as! VersionRequestResponse).version
                AppDelegate.me.settingsManager.setLastSeenVersion(major: Int(version.packageMajor), minor: Int(version.packageMinor))
                handler(.Ok, version)
            } else {
                handler(.Error, nil)
            }
        }
    }
    
    func isReadyForFWUpdate(handler: @escaping IsReadyForFWUpdateResponseHandler) {
        let message = IsReadyForFirmwareUpdate()
        commsManager.sendMessage(message: message) { (message, code, response) in
            if code == .Ok {
                let response = (response as! IsReadyForFirmwareUpdateResponse)
                handler(.Ok, response.status)
            } else {
                handler(.Error, IsReadyForFirmwareUpdateResponse.Status.Unknown)
            }
        }
    }
    
    func updateFirmware(fileURL: URL, version: KairosDeviceVersion, handler: @escaping UpdateFirmwareResponseHandler) {
        
        cancellingFirmwareUpdate = false
        do {
            let unzipDirectory = try Zip.quickUnzipFile(fileURL)
            let jsonData = try Data(contentsOf: unzipDirectory.appendingPathComponent("package_info.json"))
            let json = try JSONSerialization.jsonObject(with: jsonData) as? [String: Any]
            
            guard let majorVersionItem = json?["major_version"] as? String,
                let majorVersionInt = UInt8(majorVersionItem),
                let minorVersionItem = json?["minor_version"] as? String,
                let minorVersionInt = UInt8(minorVersionItem) else {
                    throw NSError(domain: "Error parsing JSON", code: 1, userInfo: nil)
            }
            
            guard let bootloader = json?["bootloader"] as? [String: Any],
                let bootloaderMajorItem = bootloader["major_version"] as? String,
                let bootloaderMajorInt = UInt8(bootloaderMajorItem),
                let bootloaderMinorItem = bootloader["minor_version"] as? String,
                let bootloaderMinorInt = UInt8(bootloaderMinorItem) else {
                    throw NSError(domain: "Error parsing JSON", code: 1, userInfo: nil)
            }
            
            guard let fsfw = json?["fsfw"] as? [String: Any],
                let fsfwMajorItem = fsfw["major_version"] as? String,
                let fsfwMajorInt = UInt8(fsfwMajorItem),
                let fsfwMinorItem = fsfw["minor_version"] as? String,
                let fsfwMinorInt = UInt8(fsfwMinorItem) else {
                    throw NSError(domain: "Error parsing JSON", code: 1, userInfo: nil)
            }
        
            guard let firmware = json?["firmware"] as? [String: Any],
                let firmwareMajorItem = firmware["major_version"] as? String,
                let firmwareMajorInt = UInt8(firmwareMajorItem),
                let firmwareMinorItem = firmware["minor_version"] as? String,
                let firmwareMinorInt = UInt8(firmwareMinorItem) else {
                    throw NSError(domain: "Error parsing JSON", code: 1, userInfo: nil)
            }
            
            #if FAKE2
                let newPackageVersion = KairosDeviceVersion(packageMajor: 0,
                                                            packageMinor: 0,
                                                            bootloaderMajor: 0,
                                                            bootloaderMinor: 0,
                                                            fsfwMajor: 0,
                                                            fsfwMinor: 0,
                                                            firmwareMajor: 0,
                                                            firmwareMinor: 0)
            #else
                let newPackageVersion = KairosDeviceVersion(packageMajor: majorVersionInt,
                                                            packageMinor: minorVersionInt,
                                                            bootloaderMajor: bootloaderMajorInt,
                                                            bootloaderMinor: bootloaderMinorInt,
                                                            fsfwMajor: fsfwMajorInt,
                                                            fsfwMinor: fsfwMinorInt,
                                                            firmwareMajor: firmwareMajorInt,
                                                            firmwareMinor: firmwareMinorInt)

            #endif
            
            var bootloaderCRC: UInt32 = 0
            var fsfwCRC: UInt32 = 0
            var firmwareCRC: UInt32 = 0
            var packageCRC: UInt32 = 0
            
            var bootloaderSize: UInt32 = 0
            var fsfwSize: UInt32 = 0
            var firmwareSize: UInt32 = 0
            
            var packContents: UInt8 = 0
            currentFirmwareUpdateData = [UInt8]()
            
            //Bootloader
            if newPackageVersion.bootloaderMajor > version.bootloaderMajor ||
                ((newPackageVersion.bootloaderMajor == version.bootloaderMajor) && (newPackageVersion.bootloaderMinor > version.bootloaderMinor)) {
                
                let bootloaderData = try Data(contentsOf: unzipDirectory.appendingPathComponent("bootloader.bin"))
                bootloaderSize = UInt32(bootloaderData.count)
                bootloaderCRC = calculateCRC(data: bootloaderData)
                currentFirmwareUpdateData!.append(contentsOf: bootloaderData)
                packContents = packContents | StartFirmwareUpdate.PackContent.Bootloader.rawValue
            }
            
            //FSWF
            if newPackageVersion.fsfwMajor > version.fsfwMajor ||
                ((newPackageVersion.fsfwMajor == version.fsfwMajor) && (newPackageVersion.fsfwMinor > version.fsfwMinor)) {
                
                let fsfwData = try Data(contentsOf: unzipDirectory.appendingPathComponent("fsfw.bin"))
                fsfwSize = UInt32(fsfwData.count)
                fsfwCRC = calculateCRC(data: fsfwData)
                currentFirmwareUpdateData!.append(contentsOf: fsfwData)
                packContents = packContents | StartFirmwareUpdate.PackContent.Fsfw.rawValue
            }
            
            //Firmware
            if newPackageVersion.firmwareMajor > version.firmwareMajor ||
                ((newPackageVersion.firmwareMajor == version.firmwareMajor) && (newPackageVersion.firmwareMinor > version.firmwareMinor)) {
                
                let firmwareData = try Data(contentsOf: unzipDirectory.appendingPathComponent("firmware.bin"))
                firmwareSize = UInt32(firmwareData.count)
                firmwareCRC = calculateCRC(data: firmwareData)
                currentFirmwareUpdateData!.append(contentsOf: firmwareData)
                packContents = packContents | StartFirmwareUpdate.PackContent.Firmware.rawValue
            }
            
            packageCRC = calculateCRC(data: Data(currentFirmwareUpdateData!))
            currentFirmwareUpdateDataPacket = 0
            currentFirmwareUpdateResponseHandler = handler
            currentFirmwareUpdateInfo = StartFirmwareUpdate(packageSize: UInt32(currentFirmwareUpdateData!.count),
                                                            packageCRC: packageCRC,
                                                            bootloaderSize: bootloaderSize,
                                                            bootloaderCRC: bootloaderCRC,
                                                            fsfwSize: fsfwSize,
                                                            fsfwCRC: fsfwCRC,
                                                            firmwareSize: firmwareSize,
                                                            firmwareCRC: firmwareCRC,
                                                            packContents: packContents,
                                                            versionInfo: newPackageVersion)
            
            commsManager.sendMessage(message: currentFirmwareUpdateInfo!, handler: { (message, code, response) in
                self.handleFirmwareUpdateMessage(message: message, code: code, response: response)
            })
        } catch {
            handler(.Error, 0)
        }
    }
    
    fileprivate func handleFirmwareUpdateMessage(message: KairosMessage, code: BTCommsManager.ResponseCode, response: KairosResponse?) {
        
        if code == .Ok {
            print("Current fw packet: \(currentFirmwareUpdateDataPacket)")
            if currentFirmwareUpdateDataPacket == currentFirmwareUpdateInfo!.getNumberOfPackets() {
                //FW Update finished
                print("Firmware update finished")
                AppDelegate.me.settingsManager.setLastSeenVersion(major: Int(currentFirmwareUpdateInfo!.targetVersion.packageMajor), minor: Int(currentFirmwareUpdateInfo!.targetVersion.packageMinor))
                currentFirmwareUpdateResponseHandler?(.Ok, 100)
            } else if cancellingFirmwareUpdate {
                print("Firmware update cancelled")
                let cancellationMessage = CancelFirmwareUpdate()
                commsManager.sendMessage(message: cancellationMessage, handler: { (message, code, response) in
                    if code == .Ok {
                        self.currentFirmwareUpdateResponseHandler?(.FirmwareUpdateCancelled, 0)
                    } else {
                        self.currentFirmwareUpdateResponseHandler?(.Error, 0)
                    }
                })
            } else if currentFirmwareUpdateDataPacket != (currentFirmwareUpdateInfo!.getNumberOfPackets() - 1) {
                //Regular packet
                let progress = (Double(currentFirmwareUpdateDataPacket)*100.0) / Double(currentFirmwareUpdateInfo!.getNumberOfPackets())
                currentFirmwareUpdateResponseHandler?(.Ok, progress)
                let packet = FirmwareUpdateData(sourceData: currentFirmwareUpdateData!,
                                                offset: currentFirmwareUpdateDataPacket*Int(StartFirmwareUpdate.PACKET_SIZE),
                                                length: Int(StartFirmwareUpdate.PACKET_SIZE))
                currentFirmwareUpdateDataPacket += 1
                commsManager.sendMessage(message: packet, handler: { (message, code, response) in
                    self.handleFirmwareUpdateMessage(message: message, code: code, response: response)
                })
            } else {
                //Last packet (less size)
                currentFirmwareUpdateResponseHandler?(.Ok, 99)
                let packet = FirmwareUpdateData(sourceData: currentFirmwareUpdateData!,
                                                offset: currentFirmwareUpdateDataPacket*Int(StartFirmwareUpdate.PACKET_SIZE),
                                                length: currentFirmwareUpdateInfo!.getPackageSize() % Int(StartFirmwareUpdate.PACKET_SIZE))
                currentFirmwareUpdateDataPacket += 1
                commsManager.sendMessage(message: packet, handler: { (message, code, response) in
                    self.handleFirmwareUpdateMessage(message: message, code: code, response: response)
                })
            }
        } else {
            print("Error updating firmware")
            currentFirmwareUpdateResponseHandler?(.Error, 0)
        }
    }
 
    func cancelFirmwareUpdate() {
        cancellingFirmwareUpdate = true
    }
 
    fileprivate func calculateCRC(data: Data) -> UInt32 {
        var crc: UInt32 = 0xFFFFFFFF
        let crcTable: [UInt32] = [0x00000000, 0x04C11DB7, 0x09823B6E, 0x0D4326D9, 0x130476DC, 0x17C56B6B,
                                  0x1A864DB2, 0x1E475005, 0x2608EDB8, 0x22C9F00F, 0x2F8AD6D6, 0x2B4BCB61,0x350C9B64, 0x31CD86D3, 0x3C8EA00A, 0x384FBDBD]
 
        let stream = InputStream(data: data)
        stream.open()
        let buffer = UnsafeMutablePointer<UInt8>.allocate(capacity: 4)
 
        repeat {
            let res = stream.read(buffer, maxLength: 4)
            if res == 0 || res == -1 {
                break
            }
 
            let b0 = UInt32(buffer[0])
            let b1 = UInt32(buffer[1])
            let b2 = UInt32(buffer[2])
            let b3 = UInt32(buffer[3])
 
            crc = crc ^
                ((b3 << 24 & 0xFF000000)
                    | (b2 << 16 & 0xFF0000)
                    | (b1 << 8 & 0xFF00)
                    | (b0 & 0xFF))
            
            // 0x04C11DB7 Polynomial used in STM32
            crc = (crc << 4) ^ crcTable[Int((crc >> 28) & 0xF)]
            crc = (crc << 4) ^ crcTable[Int((crc >> 28) & 0xF)]
            crc = (crc << 4) ^ crcTable[Int((crc >> 28) & 0xF)]
            crc = (crc << 4) ^ crcTable[Int((crc >> 28) & 0xF)]
            crc = (crc << 4) ^ crcTable[Int((crc >> 28) & 0xF)]
            crc = (crc << 4) ^ crcTable[Int((crc >> 28) & 0xF)]
            crc = (crc << 4) ^ crcTable[Int((crc >> 28) & 0xF)]
            crc = (crc << 4) ^ crcTable[Int((crc >> 28) & 0xF)]
        } while stream.hasBytesAvailable
        
        stream.close()
        buffer.deallocate(capacity: 4)
        
        return crc
    }
}
