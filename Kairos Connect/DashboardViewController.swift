import UIKit

class DashboardViewController: TransitableToProgressViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var updateButton: KairosWireButton!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var deviceImage: UIImageView!
    @IBOutlet weak var weatherSwitch: UISwitch!
    
    private var backgroundAnimationView: UIView?
    private var serverVersion: (Int, Int)?
    private var lastVersionCheckDate = Date(timeIntervalSince1970: 0)
    private var versionChecksInterval: TimeInterval = 60*60*24

    override func viewDidLoad() {
        super.viewDidLoad()
        
        originOfAnimationImageView = deviceImage
        
        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillEnterForeground(sender:)), name: .UIApplicationWillEnterForeground, object: nil)
        
        let device = AppDelegate.me.kairosDevice!
        titleLabel.text = device.name
        weatherSwitch.setOn(AppDelegate.me.settingsManager.weatherNotificationsEnabled, animated: false)
        updateVersionGUI()
        
        checkForNewVersion()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func applicationWillEnterForeground(sender: AnyObject) {
        if -lastVersionCheckDate.timeIntervalSinceNow >= versionChecksInterval {
            checkForNewVersion()
        }
    }
    
    @IBAction func helpClick() {
    }
    
    
    @IBAction func disconnectClick() {
        AppDelegate.me.kairosDevice?.disconnect()
        AppDelegate.me.kairosDevice = nil
    }
    
    @IBAction func updateClick() {
    }
    
    @IBAction func weatherSettingsClick() {
    }
    
    @IBAction func weatherSwitchChanged() {
        if weatherSwitch.isOn {
            AppDelegate.me.weatherManager.enableWeatherNotifications()
        } else {
            AppDelegate.me.weatherManager.disableWeatherNotifications()
        }
    }
    
    private func checkForNewVersion() {
        lastVersionCheckDate = Date()
        
        //Check for device version
        AppDelegate.me.kairosDevice!.retrieveVersion { (code, version) in
            if code == .Ok {
                self.updateVersionGUI()
            }
        }
        
        //Check for server version
        let updateManager = UpdateManager()
        updateManager.retrieveServerVersion { (code, version, fwUrl) in
            DispatchQueue.main.async {
                if code == .ServerVersionRetrieved {
                    self.serverVersion = version
                    self.updateVersionGUI()
                }
            }
        }
    }
    
    private func updateVersionGUI() {
        let deviceVersion = AppDelegate.me.settingsManager.getLastSeenVersion()
        versionLabel.text = NSLocalizedString("Firmware version", comment: "") + " \(deviceVersion.0).\(deviceVersion.1)";
        
        if let serverVersion = serverVersion {
            if ((serverVersion.0 > deviceVersion.0) ||
               ((serverVersion.0 == deviceVersion.0) && (serverVersion.1 > deviceVersion.1)))
            {
                AppDelegate.me.settingsManager.updateAvaliable = true
            } else {
                AppDelegate.me.settingsManager.updateAvaliable = false
            }
        }
        
        if AppDelegate.me.settingsManager.updateAvaliable {
            self.updateButton.isHidden = false
            UIApplication.shared.applicationIconBadgeNumber = 1
            statusLabel.text = NSLocalizedString("A firmware update is available", comment: "")
        } else {
            self.updateButton.isHidden = true
            UIApplication.shared.applicationIconBadgeNumber = 0
            statusLabel.text = NSLocalizedString("Your device is updated", comment: "")
        }
    }
    
    override func transitionAnimationStarts(reverse: Bool) {
        super.transitionAnimationStarts(reverse: reverse)
        if !reverse {
            backgroundAnimationView = UIView(frame: view.bounds)
            backgroundAnimationView?.backgroundColor = UIColor(red: 0x1B/255.0, green: 0x1b/255.0, blue: 0x1b/255.0, alpha: 1)
            backgroundAnimationView?.alpha = 0
            view.addSubview(backgroundAnimationView!)
            UIView.animate(withDuration: CustomNavigationAnimation.TRANSITION_DURATION, animations: {
                self.backgroundAnimationView!.alpha = 1
            })
        } else {
            updateVersionGUI()
            view.layoutIfNeeded()
            UIView.animate(withDuration: CustomNavigationAnimation.TRANSITION_DURATION, animations: { 
                self.backgroundAnimationView!.alpha = 0
            }, completion: { (completed) in
                self.backgroundAnimationView!.removeFromSuperview()
            })
        }
    }

}
