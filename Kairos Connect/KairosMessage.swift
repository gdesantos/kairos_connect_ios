import Foundation

class KairosMessage {
    
    var data: [UInt8]
    var timeout = 1000
    private static var frameUID: UInt8 = 1
    
    init() {
        data = []
    }
    
    init(length: UInt8) {
        data = [UInt8](repeating: 0, count: Int(length))
        
        data[1] = KairosMessage.frameUID
        if data[1] <= 125 {
            KairosMessage.frameUID = data[1] + 1
        } else {
            KairosMessage.frameUID = 1
        }
        
        data[2] = length
    }
    
    func calculateFCS() {
        for byte in data {
            data[0] ^= byte
        }
    }
    
    func getTimeout() -> Int {
        return timeout
    }
}

extension KairosMessage: Hashable {
    
    var hashValue: Int {
        return Int(data[0])
    }
    
    static func == (lhs: KairosMessage, rhs: KairosMessage) -> Bool {
        return lhs.data[1] == rhs.data[1]
    }
}
