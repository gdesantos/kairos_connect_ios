import UIKit

class CustomNavigationAnimation: NSObject, UIViewControllerAnimatedTransitioning {
    
    var transitionType: UINavigationControllerOperation = .push
    static let TRANSITION_DURATION:TimeInterval = 0.5
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return CustomNavigationAnimation.TRANSITION_DURATION
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        if transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from) is TransitableToProgressViewController {
            //We are going from transitable to progress
            let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from) as! TransitableToProgressViewController
            let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to) as! WatchProgressViewController
            
            let originRect = fromViewController.originOfAnimationImageView!.convert(fromViewController.originOfAnimationImageView!.frame, to: fromViewController.view)
            let animationImage = UIImageView(frame: originRect)
            animationImage.image = fromViewController.originOfAnimationImageView!.image
            animationImage.contentMode = .scaleAspectFit
            transitionContext.containerView.addSubview(animationImage)
            fromViewController.originOfAnimationImageView?.alpha = 0
            
            toViewController.view.isHidden = true
            transitionContext.containerView.addSubview(toViewController.view)
            toViewController.view.layoutIfNeeded()
            fromViewController.transitionAnimationStarts(reverse: false)
            toViewController.transitionAnimationStarts(reverse: false)
            
            UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
                let destinationRect = toViewController.progressView.image.superview!.convert(toViewController.progressView.image.frame, to: toViewController.view)
                animationImage.frame = destinationRect
            }) { (finished) in
                toViewController.view.isHidden = false
                fromViewController.transitionAnimationEnds(reverse: false)
                toViewController.transitionAnimationEnds(reverse: false)
                transitionContext.completeTransition(true)
            }
        } else {
            //We are going back from progress to transitable
            let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from) as! WatchProgressViewController
            let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to) as! TransitableToProgressViewController
            let originRect = fromViewController.progressView.image.superview!.convert(fromViewController.progressView.image.frame, to: fromViewController.view)
            let animationImage = UIImageView(frame: originRect)
            animationImage.image = fromViewController.progressView.image.image
            animationImage.contentMode = .scaleAspectFit
            
            fromViewController.transitionAnimationStarts(reverse: true)
            toViewController.transitionAnimationStarts(reverse: true)
            transitionContext.containerView.addSubview(toViewController.view)
            transitionContext.containerView.addSubview(animationImage)
            
            UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
                let destinationRect = toViewController.originOfAnimationImageView!.convert(toViewController.originOfAnimationImageView!.frame, to: toViewController.view)
                animationImage.frame = destinationRect
            }) { (finished) in
                animationImage.removeFromSuperview()
                toViewController.originOfAnimationImageView?.alpha = 1
                fromViewController.transitionAnimationEnds(reverse: true)
                toViewController.transitionAnimationEnds(reverse: true)
                transitionContext.completeTransition(true)
            }
        }
    }
}
