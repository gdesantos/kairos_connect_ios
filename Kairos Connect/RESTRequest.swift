import Foundation

class RESTRequest: NSObject, NSCoding {
    
    var serverURL: String
    var serviceName: String
    var method: String
    var params = [String: String]()
    var requestType = RequestType.Post
    var saveForRetry = false
    
    enum RequestType:Int {
        case Get, Post
    }

    init(serverURL: String, serviceName: String, method: String ) {
        if !serverURL.hasPrefix("http://") && !serverURL.hasPrefix("https://") {
            self.serverURL = "http://" + serverURL
        } else {
            self.serverURL = serverURL
        }
        self.serviceName = serviceName
        self.method = method
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.serverURL = aDecoder.decodeObject(forKey: "server_url") as! String
        self.serviceName = aDecoder.decodeObject(forKey: "service_name") as! String
        self.method = aDecoder.decodeObject(forKey: "method") as! String
        self.params = aDecoder.decodeObject(forKey: "params") as! [String: String]
        self.requestType = RequestType(rawValue: aDecoder.decodeInteger(forKey: "request_type"))!
        self.saveForRetry = aDecoder.decodeBool(forKey: "save_for_retry")
    }

    
    func buildParamsString() -> String {
        var result = ""
        
        for (key, value) in params {
            result += "&\(key)=\(value)"
        }
        
        return result
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(serverURL, forKey: "server_url")
        aCoder.encode(serviceName, forKey: "service_name")
        aCoder.encode(method, forKey: "method")
        aCoder.encode(params, forKey: "params")
        aCoder.encode(requestType.rawValue, forKey: "request_type")
        aCoder.encode(saveForRetry, forKey: "save_for_retry")
    }
}
