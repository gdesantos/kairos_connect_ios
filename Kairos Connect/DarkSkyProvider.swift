import Foundation
import CoreLocation

class DarkSkyProvider: WeatherProvider, WeatherProviderProtocol {
    
    func getURL(location: CLLocationCoordinate2D) -> String {
        let unitsString: String
        
        switch units {
        case .Celsius: unitsString = "&units=si"
        case .Farenheit: unitsString = "&units=us"
        }
        
        let langString: String
        if Locale.preferredLanguages.count > 0 && Locale.preferredLanguages[0] == "es-ES" {
            langString = "&lang=es"
        } else {
            langString = ""
        }
        
        return "https://api.darksky.net/forecast/050188788f1bf593b923efb528a0da8d/\(location.latitude),\(location.longitude)?exclude=minutely,hourly,daily,alerts,flags" + unitsString + langString
    }
    
    func parse(response: Data) -> String? {
        do {
            let json = try JSONSerialization.jsonObject(with: response) as? [String: Any]
            
            guard let jCurrently = json?["currently"] as? [String: Any],
                let summary = jCurrently["summary"] as? String,
                let temperature = jCurrently["temperature"] as? Double else {
                    return nil
            }
            
            let unitsString: String
            switch units {
            case .Celsius: unitsString = "C"
            case .Farenheit: unitsString = "F"
            }
            
            return summary + ", " + NSLocalizedString("Temperature", comment: "") + " \(temperature)º" + unitsString
        } catch {
            return nil
        }
    }
}
