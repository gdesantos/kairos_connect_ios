import Foundation
import CoreLocation

class WeatherProvider {
    
    enum TemperatureUnits: Int {
        case Celsius, Farenheit
    }
    
    let units: TemperatureUnits
    
    init(withUnits: TemperatureUnits) {
        units = withUnits
    }
    
    convenience init() {
        self.init(withUnits: .Celsius)
    }
}

protocol WeatherProviderProtocol {
    
    func getURL(location: CLLocationCoordinate2D) -> String
    func parse(response: Data) -> String?
}
