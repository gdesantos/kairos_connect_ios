import Foundation

class KairosResponse: KairosMessage {

    init(inputData: [UInt8]) {
        super.init()
        data = inputData
    }
    
    init(baseData: [UInt8], extendedData: [UInt8]) {
        super.init()
        data += baseData
        data += extendedData
    }
    
    static func readResponse(data: [UInt8]) -> KairosResponse? {
        if data.count < 5 {
            return nil
        }
        
        var baseData = [UInt8](repeating: 0, count: 5)
        for index in 0...4 {
            baseData[index] = data[index]
        }
        
        var extendedData: [UInt8]?
        var response: KairosResponse!
        
        switch data[4] {
        case VersionRequest.TYPE_ID:
            if data.count != 5+9 {
                return nil
            }
            extendedData = [UInt8](repeating: 0, count: 9)
            for index in 0...8 {
                extendedData?[index] = data[index+5]
            }
            response = VersionRequestResponse(baseData: baseData, extendedData: extendedData!)
            
        case IsReadyForFirmwareUpdate.TYPE_ID:
            if data.count != 5+3 {
                return nil
            }

            extendedData = [UInt8](repeating: 0, count: 3)
            for index in 0...2 {
                extendedData?[index] = data[index+5]
            }
            response = IsReadyForFirmwareUpdateResponse(baseData: baseData, extendedData: extendedData!)
            
        default:
            response = KairosResponse(inputData: baseData)
        }
        
        return response
    }
    
    func isACK() -> Bool {
        return data[3] == 0x1
    }
    
    func checkFCS() -> Bool {
        var tmpFcs = UInt8(0x0)
        
        for b in data {
            tmpFcs ^= b
        }
        
        return tmpFcs == data[0]
    }
}
