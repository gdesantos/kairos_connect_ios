import Foundation
import CoreLocation

class OpenWeatherMapProvider: WeatherProvider, WeatherProviderProtocol {
    
    func getURL(location: CLLocationCoordinate2D) -> String {
        let unitsString: String
        
        switch units {
        case .Celsius: unitsString = "units=metric"
        case .Farenheit: unitsString = "units=imperial"
        }
        
        let langString: String
        if Locale.preferredLanguages.count > 0 && Locale.preferredLanguages[0] == "es-ES" {
            langString = "&lang=es"
        } else {
            langString = ""
        }

        return "http://api.openweathermap.org/data/2.5/weather?lat=\(location.latitude)&lon=\(location.longitude)&appid=5e22728f8cfd81299420132c4277be85&" + unitsString + langString
    }
    
    func parse(response: Data) -> String? {
        do {
            let json = try JSONSerialization.jsonObject(with: response) as? [String: Any]
            
            guard let jWeather = json?["weather"] as? [Any],
                    jWeather.count > 0,
                    let jDescription = jWeather[0] as? [String: Any],
                    let description = jDescription["description"] as? String,
                    let jMain = json?["main"] as? [String: Any],
                    let temperature = jMain["temp"] as? Double else {
                return nil
            }
            
            let unitsString: String
            switch units {
            case .Celsius: unitsString = "C"
            case .Farenheit: unitsString = "F"
            }

            return description + ", " + NSLocalizedString("Temperature", comment: "") + " \(temperature)º" + unitsString
        } catch {
            return nil
        }
    }
}
