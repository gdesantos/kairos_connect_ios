import UIKit

class SplashViewController: UIViewController {
    
    @IBOutlet weak var kairosLogo: UIStackView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        kairosLogo.center.y += 100
        kairosLogo.alpha = 0
        
        UIView.animate(withDuration: 0.390, delay: 0.2, options: [], animations: { 
            self.kairosLogo.center.y -= 100
        }, completion: nil)
        
        UIView.animate(withDuration: 0.630, delay: 0.2, options: [], animations: {
            self.kairosLogo.alpha = 1
        }, completion: nil)
        
        let deadlineTime = DispatchTime.now() + .seconds(1)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
            if AppDelegate.me.kairosDevice == nil {
                if AppDelegate.me.settingsManager.showHomeActivity {
                    self.performSegue(withIdentifier: "from_splash_to_home", sender: self)
                } else {
                    self.performSegue(withIdentifier: "from_splash_to_scan", sender: self)
                }
            } else {
                self.performSegue(withIdentifier: "from_splash_to_dashboard", sender: self)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
