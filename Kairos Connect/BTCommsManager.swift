import Foundation
import CoreBluetooth

class BTCommsManager: NSObject {
    
    fileprivate var btCentralManager: CBCentralManager!
    fileprivate var btDevice: CBPeripheral!
    fileprivate var btService: CBService!
    fileprivate var btReadCharacteristic: CBCharacteristic?
    fileprivate var btWriteCharacteristic: CBCharacteristic?
    fileprivate let messageDispatchQueue = DispatchQueue(label: "message.dispatch.queue")
    fileprivate var messageQueue: [KairosMessage] = []
    fileprivate var handlerQueue: [ResponseHandler] = []
    fileprivate var disconnecting = false
    fileprivate var timeoutTimer: Timer?
    
    fileprivate static let KAIROS_SERVICE_UID = CBUUID.init(string: "F4F2B816-A092-466E-BC76-320462D6341A")
    fileprivate static let WRITE_CHARACTERISTIC_UID = CBUUID.init(string: "D0F0AECD-6405-46C1-9DD7-0B040C3B7047")
    fileprivate static let READ_CHARACTERISTIC_UID = CBUUID.init(string: "C8853E0F-C506-4835-A248-C6F0D2764AB1")
    fileprivate static let CONNECT_TIMEOUT: TimeInterval = 10
    
    enum ResponseCode {
        case Ok, ConnectionError, NACKError, GeneralError
    }
    
    typealias ResponseHandler = (_ message: KairosMessage, _ code: ResponseCode, _ response: KairosResponse?) -> Void

    init(address: UUID) {
        btCentralManager = AppDelegate.me.btCentralManager
        super.init()
        btCentralManager.delegate = self
        let peripherals = btCentralManager.retrievePeripherals(withIdentifiers: [address])
        if peripherals.count > 0 {
            btDevice = peripherals[0]
        } else {
            btDevice = nil
            print("Invalid peripheral")
        }
    }
    
    func disconnect() {
        if btDevice == nil {
            print("Invalid peripheral")
            return
        }
        
        print("Disconnecting...")
        disconnecting = true
        btCentralManager.cancelPeripheralConnection(btDevice)
    }
    
    func sendMessage(message: KairosMessage, handler: @escaping ResponseHandler) {
        print("Queuing message...")
        messageDispatchQueue.async {
            self.messageQueue.append(message)
            self.handlerQueue.append(handler)
            print("Queue size: \(self.messageQueue.count)")
            if self.messageQueue.count == 1 {
                self.sendToDevice()
            }
        }
    }
    
    fileprivate func sendToDevice() {
        if btDevice == nil {
            print("Invalid peripheral")
            self.messageProcessed(code: .GeneralError, response: nil)
            return
        }
        
        if isConnected() {
            print("Sending message...")
            //Set write timeout
            DispatchQueue.main.async {
                self.timeoutTimer = Timer.scheduledTimer(withTimeInterval: TimeInterval(self.messageQueue[0].timeout), repeats: false, block: { (timer) in
                    print("Timeout writing")
                    if self.btReadCharacteristic != nil {
                        self.btDevice?.setNotifyValue(false, for: self.btReadCharacteristic!)
                    }
                    self.messageProcessed(code: .GeneralError, response: nil)
                })
            }
            btDevice.setNotifyValue(true, for: btReadCharacteristic!)
            btDevice.writeValue(Data(self.messageQueue[0].data), for: btWriteCharacteristic!, type: CBCharacteristicWriteType.withResponse)
        } else {
            connect();
        }
    }
    
    fileprivate func connect() {
        print("Connecting...")
        //Set connection timeout
        DispatchQueue.main.async {
            self.timeoutTimer = Timer.scheduledTimer(withTimeInterval: BTCommsManager.CONNECT_TIMEOUT, repeats: false, block: { (timer) in
                print("Timeout connecting")
                self.btCentralManager.cancelPeripheralConnection(self.btDevice)
                self.messageProcessed(code: .ConnectionError, response: nil)
            })
        }
        btCentralManager.connect(self.btDevice, options: nil)
    }
    
    fileprivate func isConnected() -> Bool {
        return btDevice.state == .connected && btReadCharacteristic != nil && btWriteCharacteristic != nil
    }
    
    fileprivate func messageProcessed(code: ResponseCode, response: KairosResponse?) {
        //The message has been processed, so cancel any pending timeout timer
        timeoutTimer?.invalidate()
        handlerQueue[0](messageQueue[0], code, response)
        messageDispatchQueue.async {
            //Message processed. Remove it from the queue and continue with the next (if any)
            _ = self.messageQueue.removeFirst()
            _ = self.handlerQueue.removeFirst()
            print("Queue size: \(self.messageQueue.count)")
            if self.messageQueue.count > 0 {
                self.sendToDevice()
            }
        }
    }
}

extension BTCommsManager: CBCentralManagerDelegate {
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("Connected. Discovering service...")
        btDevice = peripheral
        btDevice.delegate = self
        btDevice.discoverServices([BTCommsManager.KAIROS_SERVICE_UID])
    }
    
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        print("Connection error")
        btReadCharacteristic = nil
        btWriteCharacteristic = nil
        messageProcessed(code: .ConnectionError, response: nil)
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        print("Device disconnected")
        btReadCharacteristic = nil
        btWriteCharacteristic = nil
        disconnecting = false
    }

}

extension BTCommsManager: CBPeripheralDelegate {
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        if error == nil {
            btService = nil
            for service in btDevice.services! {
                if service.uuid.isEqual(BTCommsManager.KAIROS_SERVICE_UID) {
                    btService = service
                    break
                }
            }
            if btService == nil {
                print("Service not found")
                messageProcessed(code: .GeneralError, response: nil)
                return
            }
            print("Service discovered. Discovering characteristics...")
            btDevice.discoverCharacteristics([BTCommsManager.READ_CHARACTERISTIC_UID, BTCommsManager.WRITE_CHARACTERISTIC_UID], for: btService)
        } else {
            print("Service discovery error")
            messageProcessed(code: .GeneralError, response: nil)
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        timeoutTimer?.invalidate()
        if error == nil {
            btReadCharacteristic = nil
            btWriteCharacteristic = nil
            for characteristic in btService.characteristics! {
                if characteristic.uuid.isEqual(BTCommsManager.READ_CHARACTERISTIC_UID) {
                    btReadCharacteristic = characteristic
                } else if characteristic.uuid.isEqual(BTCommsManager.WRITE_CHARACTERISTIC_UID) {
                    btWriteCharacteristic = characteristic
                }
            }
            if btReadCharacteristic == nil || btWriteCharacteristic == nil {
                print("Characteristics not found")
                messageProcessed(code: .GeneralError, response: nil)
                return
            }
            print("Characteristics discovered. Device ready")
            messageDispatchQueue.async {
                self.sendToDevice()
            }
        } else {
            print("Characteristics discovery error")
            messageProcessed(code: .GeneralError, response: nil)
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        if error == nil {
            print("Message sent.Reading response...")
        } else {
            print("Message fail: \(String(describing: error?.localizedDescription))")
            if btReadCharacteristic != nil {
                btDevice.setNotifyValue(false, for: btReadCharacteristic!)
            }
            messageProcessed(code: .GeneralError, response: nil)
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        timeoutTimer?.invalidate()
        //Remove notifications from this characteristic until next write operation
        btDevice.setNotifyValue(false, for: btReadCharacteristic!)
        
        guard error == nil else {
            print("Error reading response: \(String(describing: error?.localizedDescription))")
            messageProcessed(code: .GeneralError, response: nil)
            return
        }
        
        guard characteristic.value != nil else {
            print("Empty response")
            messageProcessed(code: .GeneralError, response: nil)
            return
        }
        
        let bytes = [UInt8](characteristic.value!)
        guard let response = KairosResponse.readResponse(data: bytes) else {
            print("Invalid response")
            messageProcessed(code: .GeneralError, response: nil)
            return
        }
        
        print("Response readed")
        if response.isACK() {
            print("ACK received")
            messageProcessed(code: .Ok, response: response)
        } else {
            print("NACK received")
            messageProcessed(code: .NACKError, response: nil)
        }
    }
}
